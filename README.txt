OSCAR Selfbook - Oscar online appointment booking
Tested with Oscar 14 and over


Features
---------
There are no username/password for the patients.
Any patients can take an appointment. You don't need to manage username/password of patients.
To keep the patient confidentiality, there is no feedback to the user.
Instead, an email is sent to the receptionist if the HIN is not in the system or the attending doctor is not the same as the one listed in the doctor section.
You can customize the algorithm (the questions that patients need to answer before choosing the appointments)
Support French & English for now. Other languages can be added later.
Mobile friendly.


Tan Pham
tan_oscar@rocketmail.com
2016/02/11


========================================================

To setup, you will need to configure the following files:

resources/config.xml
----------
Place your oscar server, smtp server,... credentials here


resources/demo/demobooking.xml
-----------------------
Customize the algorithm for your clinic here


resources/demo/*  
-----------
Mail templates that you need to customize


resources/string_tables
----------
string_en.properties    english language file
string_fr.properties    french language file

