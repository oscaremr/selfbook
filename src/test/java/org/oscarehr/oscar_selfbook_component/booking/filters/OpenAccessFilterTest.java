/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking.filters;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.oscar_selfbook_component.manager.OscarScheduleManager;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class OpenAccessFilterTest {

	private static final Logger logger = MiscUtils.getLogger();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	//@Test
	public void testOpenAccessFilterTest() throws Exception
	{
		OpenAccessFilter appointmentFilter = new OpenAccessFilter();
		String providerNo = "99";
		Calendar date = Calendar.getInstance();
		Calendar today = (Calendar) date.clone();
		date.add(Calendar.DAY_OF_YEAR, 1);
		Long appointmentTypeId = 1l;
		DayWorkScheduleTransfer dayWorkScheduleTransfer = OscarScheduleManager.getDayWorkSchedule(providerNo,   date);
		Character[] chars = {'o'};
		List<TimeSlot> currentlyAllowedTimeSlots = Clinic.getAllowedTimesByType(dayWorkScheduleTransfer,chars,providerNo);
		logTimeSlots("Before Filter",currentlyAllowedTimeSlots);
		Map<String,String> params = new HashMap<String,String>();
		
		Calendar newNow = (Calendar) today.clone();
		
		setNewNow(newNow, params,8,59,1);
				
		List<TimeSlot> slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		
		logTimeSlots("@8:59Today",slots);
		assertEquals("@8:59Today",0,slots.size());
		
		
		setNewNow(newNow, params,9,31,1);
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@9:31Today",slots);
		assertEquals("@9:31Today",1,slots.size());
		
		
		setNewNow(newNow, params,13,0,0);
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@13:00Today",slots);
		assertEquals("@13:00Today",8,slots.size());
		
	}
	
	
	@Test
	public void testOpenAccessFilterTest2() throws Exception
	{
		OpenAccessFilter appointmentFilter = new OpenAccessFilter();
		String providerNo = "88";
		Calendar date = Calendar.getInstance();
		Calendar today = (Calendar) date.clone();
		date.add(Calendar.DAY_OF_YEAR, 1);
		Long appointmentTypeId = 1l;
		DayWorkScheduleTransfer dayWorkScheduleTransfer = OscarScheduleManager.getDayWorkSchedule(providerNo,   date);
		Character[] chars = {'O'};
		List<TimeSlot> currentlyAllowedTimeSlots = Clinic.getAllowedTimesByType(dayWorkScheduleTransfer,chars,providerNo);
		logTimeSlots("Before Filter",currentlyAllowedTimeSlots);
		Map<String,String> params = new HashMap<String,String>();
		
		Calendar newNow = (Calendar) today.clone();
		
		setNewNow(newNow, params,8,59,1);
				
		List<TimeSlot> slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		
		logTimeSlots("@8:59Today",slots);
		//assertEquals("@8:59Today",0,slots.size());
		
		
		setNewNow(newNow, params,9,31,1);
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@9:31Today",slots);
		//assertEquals("@9:31Today",1,slots.size());
		
		
		setNewNow(newNow, params,13,0,0);
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@13:00Today",slots);
		//assertEquals("@13:00Today",8,slots.size());
		
	}
	
	
	private void setNewNow(Calendar newNow, Map<String,String> params,int hour,int minute,int second){
		newNow.set(Calendar.HOUR_OF_DAY, hour);
		newNow.set(Calendar.MINUTE, minute);
		newNow.set(Calendar.SECOND,second);
		params.put("nowDate", sdf.format(newNow.getTime())); 
	}
	
	private void logTimeSlots(String desc, List<TimeSlot> timeslots){
		logger.info(desc+" "+timeslots.size());
		for(TimeSlot timeslot: timeslots){
			logger.debug( timeslot.getAvailableApptTime().getTime() );
		}
	}
}
