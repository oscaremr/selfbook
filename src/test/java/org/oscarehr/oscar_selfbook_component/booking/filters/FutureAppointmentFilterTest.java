/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking.filters;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.oscar_selfbook_component.manager.OscarScheduleManager;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class FutureAppointmentFilterTest {
	private static final Logger logger = MiscUtils.getLogger();
	
	@Test
	public void testFutureAppointmentFilter() throws Exception{
		FutureApptFilter appointmentFilter = new FutureApptFilter();
		String providerNo = "999998";
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, 1);
		Long appointmentTypeId = 1l;
		DayWorkScheduleTransfer dayWorkScheduleTransfer = OscarScheduleManager.getDayWorkSchedule(providerNo,   date);
		Character[] chars = {'1','2'};
		List<TimeSlot> currentlyAllowedTimeSlots = Clinic.getAllowedTimesByType(dayWorkScheduleTransfer,chars,providerNo);
		Map<String,String> params = null;
		
		logTimeSlots("prefilter",currentlyAllowedTimeSlots);
		
		
		List<TimeSlot> slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		
		logTimeSlots("yesterday",slots);
		
		assertEquals(currentlyAllowedTimeSlots.size(),slots.size());
		
		params = new HashMap<String,String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar newNow = (Calendar) date.clone();
		newNow.set(Calendar.HOUR_OF_DAY, 15);
		newNow.set(Calendar.MINUTE, 59);
		newNow.set(Calendar.SECOND,55);
		
		params.put("nowDate", sdf.format(newNow.getTime())); 
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@3:59",slots);
		assertEquals("@3:59",4,slots.size());
		
		newNow.set(Calendar.HOUR_OF_DAY, 16);
		newNow.set(Calendar.MINUTE, 00);
		newNow.set(Calendar.SECOND,00);
		
		params.put("nowDate", sdf.format(newNow.getTime())); 
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@4:00",slots);
		assertEquals("@4:00",3,slots.size());
		
		params.put("buffer", "30");
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@4:00,with 30min buffer",slots);
		assertEquals("@4:00,with 30min buffer",1,slots.size());
		
		params.put("buffer", "29");
		slots = appointmentFilter.filterAvailableTimeSlots(null,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		logTimeSlots("@4:00,with 29min buffer",slots);
		assertEquals("@4:00,with 29min buffer",2,slots.size());
		
		
	}
	
	private void logTimeSlots(String desc, List<TimeSlot> timeslots){
		logger.info(desc+" "+timeslots.size());
		for(TimeSlot timeslot: timeslots){
			logger.debug( timeslot.getAvailableApptTime().getTime() );
		}
	}

}
