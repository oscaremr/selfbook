/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking.filters;

import static org.junit.Assert.*;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.oscar_selfbook_component.manager.OscarScheduleManager;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class SufficientContiguousTimeFilterTest {
	private static final Logger logger = MiscUtils.getLogger();
	
	
	@Test
	public void testSufficientContiguousTimeFilter() throws Exception{
		
		//____________________________________2222OOOOOOTT____OOOO222222OOTTTT____________________________// unfiltered 12 'O' appts
		//____________________________________2222OOO___TT____O___222222__TTTT____________________________// filtered to eliminate where 60 mins blocks can't fit = 4
		//                                          XXXX
		InputStream is = SufficientContiguousTimeFilterTest.class.getResourceAsStream("/clinicBooking.xml");
		Clinic clinic = Clinic.fromStream(is);
		
		SufficientContiguousTimeFilter appointmentFilter = new SufficientContiguousTimeFilter();
		String providerNo = "88";
		Calendar date = Calendar.getInstance();
		Long appointmentTypeId = 2l;
		DayWorkScheduleTransfer dayWorkScheduleTransfer = OscarScheduleManager.getDayWorkSchedule(providerNo,   date);
		Character[] chars = {'O'};
		List<TimeSlot> currentlyAllowedTimeSlots = Clinic.getAllowedTimesByType(dayWorkScheduleTransfer,chars,providerNo);
		Map<String,String> params = null;
		
		List<TimeSlot> slots = appointmentFilter.filterAvailableTimeSlots(clinic,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		
		logger.info("start :"+currentlyAllowedTimeSlots.size()+" filtered to :"+slots.size());
		
		for(TimeSlot timeslot: currentlyAllowedTimeSlots){
			logger.debug( timeslot.getAvailableApptTime().getTime() );
		}
		
		for(TimeSlot timeslot: slots){
			logger.debug( timeslot.getAvailableApptTime().getTime() );
		}
		assertEquals(4  , slots.size());
	}
	
	@Test
	public void testSufficientContiguousTimeFilterDifferentCodes() throws Exception{
		
		//____________________________________2222OOOOOOTT____OOOO222222OOTTTT____________________________// unfiltered 22 'O' and '2' appts
		//____________________________________2222OOO___TT____OOOO22222___TTTT____________________________// filtered to eliminate where 60 mins blocks can't fit = 16 
		//                                                            XXXX
		InputStream is = SufficientContiguousTimeFilterTest.class.getResourceAsStream("/clinicBooking.xml");
		Clinic clinic = Clinic.fromStream(is);
		
		SufficientContiguousTimeFilter appointmentFilter = new SufficientContiguousTimeFilter();
		String providerNo = "88";
		Calendar date = Calendar.getInstance();
		Long appointmentTypeId = 2l;
		DayWorkScheduleTransfer dayWorkScheduleTransfer = OscarScheduleManager.getDayWorkSchedule(providerNo,   date);
		Character[] chars = {'2','O'};  //Make sure this is sorted!
		List<TimeSlot> currentlyAllowedTimeSlots = Clinic.getAllowedTimesByType(dayWorkScheduleTransfer,chars,providerNo);
		Map<String,String> params = null;
		
		List<TimeSlot> slots = appointmentFilter.filterAvailableTimeSlots(clinic,providerNo,providerNo,appointmentTypeId,dayWorkScheduleTransfer,currentlyAllowedTimeSlots, date,params);
		
		logger.info("start :"+currentlyAllowedTimeSlots.size()+" filtered to :"+slots.size());
		
		for(TimeSlot timeslot: currentlyAllowedTimeSlots){
			logger.debug( timeslot.getAvailableApptTime().getTime() );
		}
		
		for(TimeSlot timeslot: slots){
			logger.debug( timeslot.getAvailableApptTime().getTime() );
		}
		
		
		assertEquals(16  , slots.size());
		
		
		
		
		
	}
	
	
	
	
	
}
