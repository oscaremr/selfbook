/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.manager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.oscarehr.ws.AppointmentTransfer;
import org.oscarehr.ws.CalendarScheduleCodePairTransfer;
import org.oscarehr.ws.DayWorkScheduleTransfer;


public class OscarScheduleManager {

			public static void zeroTimeFields(Calendar cal)
			{
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				cal.getTimeInMillis();
			}
	
			public static DayWorkScheduleTransfer  getDayWorkSchedule(String providerNo,   Calendar date){
				DayWorkScheduleTransfer dayWorkScheduleTransfer = new DayWorkScheduleTransfer();
				dayWorkScheduleTransfer.setHoliday(false);
				String[] timecode = new String[5];
				
				timecode[0] = "____________________________________111122111111____1111111111111111____________________________";
				timecode[1] = "________________________________11112211s1s11122____22111s1s11111111____________________________";
				timecode[2] = "__________________________________1111ooooooooLP____1111ooooooooLP______________________________";
				timecode[3] = "____________________________________2222OOOOOOTT____OOOO222222OOTTTT____________________________";
				timecode[4] = "____________________________________6666________________________________________________________";
				
			
				int tc = 0;
				if("91111".equals(providerNo)){
					tc = 1;
				}else if("99".equals(providerNo)){
					tc = 2;
				}else if("88".equals(providerNo)){
					tc = 3;
				}else if("77".equals(providerNo)){
					tc=4;
				}
				
				int timeSlotDuration=(60 * 24) / timecode[tc].length();
				dayWorkScheduleTransfer.setTimeSlotDurationMin(timeSlotDuration);
				
				List<CalendarScheduleCodePairTransfer> calPairs = dayWorkScheduleTransfer.getTimeSlots();
				//CalendarScheduleCodePairTransfer calTran = new CalendarScheduleCodePairTransfer();
				
				// sort out designated timeslots and their purpose
				Calendar timeSlot=(Calendar) date.clone();
				zeroTimeFields(timeSlot);
								
				for (int i=0; i<timecode[tc].length(); i++)
				{
					// ignore _ because that's a blank place holder identifier... also not my fault, just processing what's been already written.
					if ('_'!=timecode[tc].charAt(i))
					{
						CalendarScheduleCodePairTransfer calendarScheduleCodePairTransfer = new CalendarScheduleCodePairTransfer();
						calendarScheduleCodePairTransfer.setDate((Calendar) timeSlot.clone());
						calendarScheduleCodePairTransfer.setScheduleCode((int)timecode[tc].charAt(i));
						calPairs.add(calendarScheduleCodePairTransfer);					}
					
					timeSlot.add(GregorianCalendar.MINUTE, timeSlotDuration);
				}

				return dayWorkScheduleTransfer;
			}
			
			public static List<AppointmentTransfer> getAppointments(String providerNo, Calendar date){
				List<AppointmentTransfer> list = new ArrayList<AppointmentTransfer>();
				
				AppointmentTransfer aptf = new AppointmentTransfer();
				//int year = 2013;
				//int month = 1;
				//int day = 1;
				if("88".equals(providerNo)){
					int hourOfDay =13;
					int minute = 0;
					int second = 0;
					int milliSecond = 0;
					Calendar date1 = (Calendar) date.clone();
					date1.set(Calendar.HOUR_OF_DAY, hourOfDay);
					date1.set(Calendar.MINUTE,minute);
					date1.set(Calendar.SECOND,second);
					date1.set(Calendar.MILLISECOND, milliSecond);
					aptf.setAppointmentStartDateTime(date1);
					Calendar date2 = (Calendar) date1.clone();
					date2.set(Calendar.MINUTE, 29);
					date2.set(Calendar.SECOND, 59);
					
					aptf.setAppointmentEndDateTime(date2);
					list.add(aptf);
				}else{
					int hourOfDay =13;
					int minute = 0;
					int second = 0;
					int milliSecond = 0;
					Calendar date1 = (Calendar) date.clone();
					date1.set(Calendar.HOUR_OF_DAY, hourOfDay);
					date1.set(Calendar.MINUTE,minute);
					date1.set(Calendar.SECOND,second);
					date1.set(Calendar.MILLISECOND, milliSecond);
					aptf.setAppointmentStartDateTime(date1);
					Calendar date2 = (Calendar) date1.clone();
					date2.set(Calendar.MINUTE, 14);
					date2.set(Calendar.SECOND, 59);
					aptf.setAppointmentEndDateTime(date2);
					list.add(aptf);
				}
				return list;
			}
				
	
}
