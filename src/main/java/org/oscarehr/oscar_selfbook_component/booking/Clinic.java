/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.manager.AppointmentType;
import org.oscarehr.util.XmlUtils;
import org.oscarehr.ws.CalendarScheduleCodePairTransfer;
import org.oscarehr.ws.DayWorkScheduleTransfer;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.oscar_selfbook_component.utils.EncryptionUtils;
import org.oscarehr.util.ConfigXmlUtils;

public class Clinic {

    private static final Logger logger = MiscUtils.getLogger();
    String name = null;
    List<AppointmentType> appointmentTypes = null;
    List<String> debugUsers = null;
    Map<Character, Integer> appointmentCodeDurations = null;
    Map<String, Provider> providers = null;
    Map<String, Algorithm> algorithms = null;

    String title = null;
    String address = null;
    String hours = null;
    String phone = null;
    String rssFeed = null;
    Integer daysToSearchAheadLimit = 10;
    Integer numberOfAppointmentOptionsToReturn = 10;
    Boolean appointmentBookingOnline = false;
    String mainWebLink = null;
    String logoFileName = null;
    String timeZone= null;

    Map<Integer, String> algDisplayText = null;
    Map<Integer, String> algAnswerType = null;
    Map<Integer, String> algAnswerRadioText = null;
    String walkInDoctor = null;
    String noOnlineBooking = null;
    String displayMessageCanAlsoCallToBookAppt=null;
    String location = null;

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getWalkInDoctor() {
        return walkInDoctor;
    }
    
    public String getNoOnlineBooking() {
        return noOnlineBooking;
    }
    
    public String getLocation() {
        return location;
    }
    
    public String getDisplayMessageCanAlsoCallToBookAppt() {
        return displayMessageCanAlsoCallToBookAppt;
    }
    
    public String getTimeZone() {
        return timeZone;
    }

    public Provider getProvider(String mrp, String providerNo) {
        if (mrp != null && providers.get(mrp) != null) {
            Provider p = providers.get(mrp);
            if (p.getProviderNo().equals(providerNo)) {
                return p;
            }
            //If the mrp isn't what is being requested, check their team
            for (Provider teamMember : p.getTeamMembers()) {
                if (teamMember.getProviderNo().equals(providerNo)) {
                    return teamMember;
                }
            }
        }
        return null;
    }

    public Map<String, Provider> getAllProviders() {

        return providers;
    }

    public Map<String, Algorithm> getAllAlgorithms() {

        return algorithms;
    }

    public Integer getAppointmentDuration(String mrp, String provider, Long appointmentId, Character code) {
        Integer appointmentDuration = appointmentCodeDurations.get(code);
        logger.debug("mrp " + mrp + " prov " + provider + " appt " + appointmentId + " dur " + appointmentDuration);
        if (mrp != null && mrp.equals(provider)) {
            appointmentDuration = getAppointmentDuration(provider, appointmentId, code);
        } else {
            if (providers.get(mrp) != null) {
                Provider p = providers.get(mrp);
                for (Provider teamMember : p.getTeamMembers()) {
                    if (provider.equals(teamMember.getProviderNo()) && teamMember.appointmentDurations.get(appointmentId) != null) {
                        appointmentDuration = teamMember.getAppointmentDurations().get(appointmentId);
                        logger.debug("---team prov " + provider + " appt " + appointmentId + " dur " + appointmentDuration);
                    }
                }
            }
        }
        return appointmentDuration;
    }

    //BUG:doesnot look at team me
    public Integer getAppointmentDuration(String provider, Long appointmentId, Character code) {
        Integer appointmentDuration = appointmentCodeDurations.get(code);
        logger.debug("prov " + provider + " appt " + appointmentId + " dur " + appointmentDuration);
        if (providers.get(provider) != null && providers.get(provider).getAppointmentDurations() != null && providers.get(provider).getAppointmentDurations().get(appointmentId) != null) {
            appointmentDuration = providers.get(provider).getAppointmentDurations().get(appointmentId);
            logger.debug("---prov " + provider + " appt " + appointmentId + " dur " + appointmentDuration);
        }
        return appointmentDuration;
    }

    public AppointmentType getAppointmentType(Long id) {
        for (AppointmentType ap : appointmentTypes) {
            if (id == ap.getId()) {
                return ap;
            }
        }
        return null;
    }



    public String encrypt(TimeSlot toEncyrpt) throws Exception {
        String combinedString = toEncyrpt.getAvailableApptTime().getTimeInMillis() + ":" + toEncyrpt.getProviderNo() + ":" + toEncyrpt.getAppointmentType() + ":" + toEncyrpt.getCode() + ":" + toEncyrpt.getNam().replaceAll(":","") + ":" + toEncyrpt.getRepQuest().replaceAll(":","");
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        return myEncryptionUtils.encrypt(combinedString);
    }

    public TimeSlot decryptTimeSlot(String toDecrypt) throws Exception {
        TimeSlot timeslot = new TimeSlot();
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        String combinedString = myEncryptionUtils.decrypt(toDecrypt);
        //
        String[] combined = combinedString.split(":");
        String timeMsStr = combined[0];
        long timeMs = Long.parseLong(timeMsStr);
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(timeMs);

        String providerId = combined[1];
        
        Long appointmentTypeId = Long.parseLong(combined[2]);

        timeslot.setAvailableApptTime(cal);
        timeslot.setProviderNo(providerId);
        timeslot.setAppointmentType(appointmentTypeId);
        if (combined[3].length() > 0) {
            Character code = combined[3].charAt(0);
            timeslot.setCode(code);
        }
        String nam = combined[4];
        timeslot.setNam(nam);
        String repQuest = combined[5];
        timeslot.setRepQuest(repQuest);
        return timeslot;
    }

    public Boolean isProviderAcceptingAppointments(String providerNo) {
        logger.debug("is Provider Accepting Appt " + providerNo + " " + providers.get(providerNo));
        if (providers.get(providerNo) != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<AppointmentType> getAppointmentForProvider(String provider) {
        logger.debug("getting appts for clinic " + name + " for provider " + provider + " appts " + appointmentTypes);
        return appointmentTypes;
    }

    public Set<String> getProviderNo() {
        return providers.keySet();
    }

    public Map<Provider, Character[]> getProvidersForAppointmentType(String providerAlias, Long appointmentTypeId) {
        Map<Provider, Character[]> map = new HashMap<Provider, Character[]>();
        Provider provider = providers.get(providerAlias);
        Character[] codes = provider.appointmentTypes.get("" + appointmentTypeId);
        logger.debug("provider " + providerAlias + "  codes " + codes);
        if (codes != null && codes.length > 0) {
            logger.debug("putting provider " + provider + "  codes " + codeList(codes));
			//Arrays.sort(codes);
            //logger.debug("putting provider " + provider + "  sorted codes " + codeList(codes));
            map.put(provider, codes);
        }
        for (Provider teamMember : provider.getTeamMembers()) {
            Character[] codes2 = teamMember.appointmentTypes.get("" + appointmentTypeId);
            logger.debug("team " + teamMember.getProviderNo() + "  codes2 " + codes2);
            if (codes2 != null && codes2.length > 0) {
                //Arrays.sort(codes2);
                map.put(teamMember, codes2);
            }
        }
        return map;
    }

	//static methods
    public static String codeList(Character[] codes) {
        if (codes == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        for (Character code : codes) {
            sb.append(code + ",");
        }
        return sb.toString();
    }

    public static ArrayList<Calendar> getAllowedTimesByType(DayWorkScheduleTransfer dayWorkScheduleTransfer, Character[] codes) {
        // allowed time codes
        ArrayList<Calendar> allowedTimesFilteredByType = new ArrayList<Calendar>();
        for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots()) {
            Integer i = entry.getScheduleCode();
            char c = (char) (i.intValue());
            logger.debug("222222>>> " + c + " : " + codeList(codes) + " = " + Arrays.binarySearch(codes, c));
            if (Arrays.binarySearch(codes, c) >= 0) {
                allowedTimesFilteredByType.add(entry.getDate());
            }
        }
        return allowedTimesFilteredByType;
    }

    public static List<TimeSlot> getAllowedTimesByType(DayWorkScheduleTransfer dayWorkScheduleTransfer, Character[] codes, String providerNo) {
        // allowed time codes
        ArrayList<TimeSlot> allowedTimesFilteredByType = new ArrayList<TimeSlot>();
        for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots()) {
            Integer i = entry.getScheduleCode();
            char c = (char) (i.intValue());
            logger.debug("222222>>> " + c + " : " + codeList(codes) + " = " + Arrays.binarySearch(codes, c));
            if (Arrays.binarySearch(codes, c) >= 0) {
                allowedTimesFilteredByType.add(new TimeSlot(providerNo, null, entry.getDate(), c));
            }
        }
        return allowedTimesFilteredByType;
    }

    public static Clinic getClinic() throws Exception {
        String configFile = ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "booking_config_file");
        return fromConfigFile(configFile);
    }

    public static Clinic fromConfigFile(String configFile) throws Exception {
        Document doc = XmlUtils.toDocumentFromFile(configFile);
        return fromDocument(doc);
    }

    public static Clinic fromStream(InputStream is) throws Exception {
        Document doc = XmlUtils.toDocument(is);
        return fromDocument(doc);
    }

    public static Clinic fromDocument(Document doc) throws Exception {
        Clinic returnClinic = new Clinic();

        Node rootNode = doc.getFirstChild();
        returnClinic.name = XmlUtils.getAttributeValue(rootNode, "name");
        logger.debug("parsed name " + returnClinic.name);
        returnClinic.title = XmlUtils.getChildNodeTextContents(rootNode, "title");
        returnClinic.address = XmlUtils.getChildNodeTextContents(rootNode, "address");
        returnClinic.phone = XmlUtils.getChildNodeTextContents(rootNode, "phone");
        returnClinic.hours = XmlUtils.getChildNodeTextContents(rootNode, "hours");
        returnClinic.rssFeed = XmlUtils.getChildNodeTextContents(rootNode, "rssfeed");
        returnClinic.mainWebLink = XmlUtils.getChildNodeTextContents(rootNode, "mainweblink");
        returnClinic.logoFileName = XmlUtils.getChildNodeTextContents(rootNode, "logoFileName");
        returnClinic.walkInDoctor = XmlUtils.getChildNodeTextContents(rootNode, "walkInDoctor");
        returnClinic.noOnlineBooking = XmlUtils.getChildNodeTextContents(rootNode, "noOnlineBooking");
        returnClinic.displayMessageCanAlsoCallToBookAppt = XmlUtils.getChildNodeTextContents(rootNode, "displayMessageCanAlsoCallToBookAppt");
        returnClinic.timeZone = XmlUtils.getChildNodeTextContents(rootNode, "timeZone");
        returnClinic.location = XmlUtils.getChildNodeTextContents(rootNode, "location");

        String daysToSearchAheadLimitStr = XmlUtils.getChildNodeTextContents(rootNode, "daysToSearchAheadLimit");
        try {
            returnClinic.daysToSearchAheadLimit = Integer.parseInt(daysToSearchAheadLimitStr);
        } catch (Exception daysToSearchAheadLimitException) {
            logger.error("Error parsing daysToSearchAheadLimit" + daysToSearchAheadLimitStr, daysToSearchAheadLimitException);
        }

        String numberOfAppointmentOptionsToReturnStr = XmlUtils.getChildNodeTextContents(rootNode, "numberOfAppointmentOptionsToReturn");
        try {
            returnClinic.numberOfAppointmentOptionsToReturn = Integer.parseInt(numberOfAppointmentOptionsToReturnStr);
        } catch (Exception numberOfAppointmentOptionsToReturnException) {
            logger.error("Error parsing numberOfAppointmentOptionsToReturn" + numberOfAppointmentOptionsToReturnStr, numberOfAppointmentOptionsToReturnException);
        }

        returnClinic.appointmentTypes = new ArrayList<AppointmentType>();
        List<Node> apptNodes = XmlUtils.getChildNodes(rootNode, "appointment_type");

        for (Node apptNode : apptNodes) {
            logger.debug("adding appt type");
            returnClinic.appointmentTypes.add(AppointmentType.fromXml2(apptNode));
        }

        returnClinic.debugUsers = XmlUtils.getChildNodesTextContents(rootNode, "debuguser");
        logger.debug("debug User size =" + returnClinic.debugUsers.size());

        for (String s : returnClinic.debugUsers) {
            logger.debug("added debug User =" + s);
        }

        returnClinic.appointmentCodeDurations = new HashMap<Character, Integer>();
        List<Node> apptCodeNodes = XmlUtils.getChildNodes(rootNode, "appointment_code");
        for (Node apptCodeNode : apptCodeNodes) {
            Integer duration = Integer.parseInt(XmlUtils.getAttributeValue(apptCodeNode, "duration"));
            String s = XmlUtils.getAttributeValue(apptCodeNode, "code");
            logger.debug("code " + s);
            s = StringUtils.trimToNull(s);
            if (s != null && s.length() > 0) {
                logger.debug("adding " + s.charAt(0) + " with dur " + duration);
                returnClinic.appointmentCodeDurations.put(s.charAt(0), duration);
            }
        }

        returnClinic.providers = new HashMap<String, Provider>();
        List<Node> providerNodes = XmlUtils.getChildNodes(rootNode, "allowedProvider");
        for (Node providerNode : providerNodes) {
            Provider provider = Provider.fromXml(providerNode);
            returnClinic.providers.put(provider.getProviderAlias(), provider);
        }

        returnClinic.algDisplayText = new HashMap<Integer, String>();
        returnClinic.algAnswerType = new HashMap<Integer, String>();

        returnClinic.algorithms = new HashMap<String, Algorithm>();
        List<Node> algorithmNodes = XmlUtils.getChildNodes(rootNode, "algorithm");
        for (Node algorithmNode : algorithmNodes) {
            Algorithm algorithm = Algorithm.fromXml(algorithmNode);
            returnClinic.algorithms.put(algorithm.getQuestionNo(), algorithm);
        }

        for (Node algorithmNode : algorithmNodes) {
            Integer questionNo = Integer.parseInt(XmlUtils.getAttributeValue(algorithmNode, "questionNo"));
            String s = XmlUtils.getAttributeValue(algorithmNode, "displayText");
            s = StringUtils.trimToNull(s);
            if (s != null && s.length() > 0) {
                returnClinic.algDisplayText.put(questionNo, s);
            }
            s = XmlUtils.getAttributeValue(algorithmNode, "answerType");
            s = StringUtils.trimToNull(s);
            if (s != null && s.length() > 0) {
                returnClinic.algAnswerType.put(questionNo, s);
            }

        }

        return returnClinic;
    }

    public List<String> getDebugUsers() {
        return debugUsers;
    }

    public String getHours() {
        return hours;
    }

    public String getMainWebLink() {
        return mainWebLink;
    }

    public String getLogoFileName() {
        return logoFileName;
    }

    public String getAlgDisplayText(Integer questionNo) {
        return algDisplayText.get(questionNo);
    }

    public String getAlgAnswerType(Integer questionNo) {
        return algAnswerType.get(questionNo);
    }

    public Integer getAlgSize() {
        return algDisplayText.size();
    }

    public Integer getDaysToSearchAheadLimit() {
        return daysToSearchAheadLimit;
    }

    public Integer getNumberOfAppointmentOptionsToReturn() {
        return numberOfAppointmentOptionsToReturn;
    }

    public void setAppointmentBookingOnline(Boolean appointmentBookingOnline) {
        this.appointmentBookingOnline = appointmentBookingOnline;
    }

    public Boolean isAppointmentBookingOnline() {
        return this.appointmentBookingOnline;
    }

}
