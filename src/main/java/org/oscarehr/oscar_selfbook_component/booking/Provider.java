/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.XmlUtils;
import org.w3c.dom.Node;

/*
 * ...
<allowedProvider providerNo="999998">
	<allowed_appointment id="1" appointment_codes="o,1"/>
	<allowed_appointment id="2" appointment_codes="o,1"/>
	<allowed_appointment id="3" appointment_codes="o,1"/>
	<team>
		<member providerNo="91111" role>
			<allowed_appointment id="4" appointment_codes="o,1"/>
		</member>	
	</team> 
 */
public class Provider{
	private static final Logger logger = MiscUtils.getLogger();
	String providerNo = null;
	Map<String, Character[]> appointmentTypes = null;
	Map<Long, Integer> appointmentDurations = null;
	List<Provider> teamMembers = null;
	List<FilterDefinition> filters = null;
	String role = null;
        String providerName = null;
        String providerAlias = null;
        
	public String getProviderNo(){
		return providerNo;
	}
        
        public String getProviderName(){
		return providerName;
	}
        
        public String getProviderAlias(){
		return providerAlias;
	}
        
	public List<FilterDefinition> getFilter(){
		return filters;
	}

	Map<Long, Integer> getAppointmentDurations(){
		return appointmentDurations;
	}

	Map<String, Character[]> getAppointmentTypes(){
		return appointmentTypes;
	}

	List<Provider> getTeamMembers(){
		return teamMembers;
	}
	
	private static List<FilterDefinition> getFilterArray(Node node){
		List<Node> filterNodes = XmlUtils.getChildNodes(node, "filter");
		List<FilterDefinition> returnVal = new ArrayList<FilterDefinition>();
		for(Node n:filterNodes){
			returnVal.add(FilterDefinition.fromXml(n));
		}	
		return returnVal;
	}

	//Allowed appointments  // need corresponding codes too

	public static Provider fromXml(Node node){
		Provider provider = new Provider();
		provider.providerNo = XmlUtils.getAttributeValue(node, "providerNo");
		provider.role = XmlUtils.getAttributeValue(node, "role");
                provider.providerName = XmlUtils.getAttributeValue(node, "name");
                provider.providerAlias = XmlUtils.getAttributeValue(node, "alias");
                if(provider.providerAlias==null) {provider.providerAlias=provider.providerNo;}
                 
		List<Node> apptNodes = XmlUtils.getChildNodes(node, "allowed_appointment");

		//provider.filters = XmlUtils.getChildNodesTextContents(node, "filter");
		provider.filters = getFilterArray(node);
                
                
		provider.appointmentTypes = getAllowedAppointments(apptNodes);
		provider.appointmentDurations = getAppointmentDurations(apptNodes);

		provider.teamMembers = new ArrayList<Provider>();
		if(XmlUtils.getChildNode(node, "team") != null){
			List<Node> members = XmlUtils.getChildNodes(XmlUtils.getChildNode(node, "team"), "member");
			for (Node memberNode : members){
				Provider providerMember = new Provider();
				providerMember.providerNo = XmlUtils.getAttributeValue(memberNode, "providerNo");
				providerMember.role = XmlUtils.getAttributeValue(memberNode, "role");
                                providerMember.providerName = XmlUtils.getAttributeValue(memberNode, "name");
                                providerMember.providerAlias = XmlUtils.getAttributeValue(memberNode, "alias");
                                if(providerMember.providerAlias==null) {providerMember.providerAlias=providerMember.providerNo;}
				List<Node> memberApptNodes = XmlUtils.getChildNodes(memberNode, "allowed_appointment");
	
				if (XmlUtils.getChildNodesTextContents(memberNode, "filter") != null && XmlUtils.getChildNodesTextContents(memberNode, "filter").size() > 0){
					providerMember.filters = getFilterArray(memberNode); 
							//XmlUtils.getChildNodesTextContents(memberNode, "filter");
				}else{
					providerMember.filters = getFilterArray(node); 
							//XmlUtils.getChildNodesTextContents(node, "filter");
				}
	
				providerMember.appointmentTypes = getAllowedAppointments(memberApptNodes);
				providerMember.appointmentDurations = getAppointmentDurations(memberApptNodes);
				provider.teamMembers.add(providerMember);
			}
		}
		return provider;
	}

	private static Map<Long, Integer> getAppointmentDurations(List<Node> apptNodes){
		Map<Long, Integer> map = new HashMap<Long, Integer>();
		for (Node allowedAppointment : apptNodes){
			String apptId = null;
			String apptDuration = null;
			try{
				apptId = XmlUtils.getAttributeValue(allowedAppointment, "id");
				apptDuration = XmlUtils.getAttributeValue(allowedAppointment, "duration");
				if(apptId != null && apptDuration != null){
					map.put(Long.parseLong(apptId), Integer.parseInt(apptDuration));
				}
			}catch (Exception e){
				logger.debug("Not a Integer  id " + apptId + " dur " + apptDuration, e);
			}
		}
		return map;
	}

	private static Map<String, Character[]> getAllowedAppointments(List<Node> apptNodes){
		Map<String, Character[]> map = new HashMap<String, Character[]>();

		for (Node allowedAppointment : apptNodes){
			String apptId = XmlUtils.getAttributeValue(allowedAppointment, "id");
			String apptCodesTemp = XmlUtils.getAttributeValue(allowedAppointment, "appointment_codes");
			String[] tempSplit = apptCodesTemp.split(",");

			Character[] allowableTimeCodes = new Character[tempSplit.length];
			int count = 0;
			for (String s : tempSplit){
				s = StringUtils.trimToNull(s);
				if (s != null && s.length() > 0){
					allowableTimeCodes[count] = s.charAt(0);
					count++;
				}
			}

			for (Character character : allowableTimeCodes){
				logger.debug("--- " + character);
			}

			Arrays.sort(allowableTimeCodes);
			//char[] appointmentCodes = new char[apptCodes.length];

			map.put(apptId, allowableTimeCodes);
		}
		return map;
	}
}
