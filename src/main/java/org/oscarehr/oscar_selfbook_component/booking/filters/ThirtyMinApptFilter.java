/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking.filters;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.oscar_selfbook_component.manager.BookingLearningManager;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class ThirtyMinApptFilter implements AvailableTimeSlotFilter{

	@Override
	public List<TimeSlot> filterAvailableTimeSlots(Clinic clinic,String mrp,String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, List<TimeSlot> currentlyAllowedTimeSlots, Calendar date,Map<String,String> params){
		ArrayList<TimeSlot> allowedTimesFilteredByFutureTimes = new ArrayList<TimeSlot>();

		Calendar myDate=new GregorianCalendar();
                int minutes;
		for (TimeSlot startTime : currentlyAllowedTimeSlots){
                    myDate.setTime(startTime.getAvailableApptTime().getTime());
                    minutes=myDate.get(Calendar.MINUTE);
			if (minutes==0 || minutes==30){
				allowedTimesFilteredByFutureTimes.add(startTime);
			}
		}
		
		if(allowedTimesFilteredByFutureTimes.size() == 0){
			BookingLearningManager.recommendDayToBeSkipped(clinic, providerId, date, appointmentTypeId, this.getClass().getName());
		}
		
		return allowedTimesFilteredByFutureTimes;
		

	}
	
	
	
}
