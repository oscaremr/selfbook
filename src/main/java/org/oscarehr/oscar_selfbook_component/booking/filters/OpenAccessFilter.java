/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking.filters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.ws.CalendarScheduleCodePairTransfer;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class OpenAccessFilter implements AvailableTimeSlotFilter{
	private static final Logger logger = MiscUtils.getLogger();
	//public ArrayList<Calendar> filterAvailableTimeSlots(String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, ArrayList<Calendar> currentlyAllowedTimeSlots)
	
	@Override
	public List<TimeSlot> filterAvailableTimeSlots(Clinic clinic,String mrp,String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, List<TimeSlot> currentlyAllowedTimeSlots, Calendar date,Map<String,String> params){
		// allowed time codes
            		Calendar now = new GregorianCalendar(); //Need to adjust for preferences; ie 30min buffer, 1 hour buffer
		
		setBuffer(params, now);
                
		ArrayList<TimeSlot> filteredResults = new ArrayList<TimeSlot>();
		for (TimeSlot entry : currentlyAllowedTimeSlots){
			if (isAllowedTime(dayWorkScheduleTransfer, entry.getAvailableApptTime(),params,now)){
				filteredResults.add(entry);
			}
		}
		return(filteredResults);
	}

	private boolean isAllowedTime(DayWorkScheduleTransfer dayWorkScheduleTransfer, Calendar timeSlot,Map<String,String> params,Calendar now){
		// if the time is open access time slot
		//    the time slot must be with in 24 hours of right now
		// else it's allowed
		char[] openAccessCodes = {'A'};
		if(params != null && params.get("codes") != null){
			openAccessCodes = params.get("codes").toCharArray();
			Arrays.sort(openAccessCodes);
		}
				
		
		for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots()){
			// find the time slot type for this entry
			if (entry.getDate().equals(timeSlot)){
				Integer i = entry.getScheduleCode();
				char c = (char) (i.intValue());

				// if it's an open access timeslot
				if (Arrays.binarySearch(openAccessCodes,c) >= 0){
					// it's allowed if it's with in 24 hours
					// it's not allowed if it's anything else
					if(params != null){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						logger.debug(sdf.format(entry.getDate().getTime())+" < "+params.get("nowDate")+" = "+(entry.getDate().getTimeInMillis() < (now.getTimeInMillis())));
					}
					return(entry.getDate().getTimeInMillis() < (now.getTimeInMillis()));
				}
			}
		}

		return(true);
	}
		
	public void setBuffer(Map<String,String> params, Calendar now){
		String buffer = null;
		if(params != null && params.get("daysbuffer") != null){
			try{
				buffer = params.get("daysbuffer");
				logger.debug("daysBuffer = "+buffer);
				
				now.add(Calendar.DATE, Integer.parseInt(buffer));
			}catch(Exception e){
				logger.error("daysbuffer "+buffer+" is not parsing correctly. Needs to be an int in minutes ",e);
			}
		}
	}
	//Used for testing to be able to adjust when "now" is
	public long getCurrentTimeMillis(Map<String,String> params){
		String nowDate = null;
		if(params != null && params.get("nowDate") != null){
			try{
				nowDate = params.get("nowDate");
				logger.debug("nowDate = "+nowDate);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				return sdf.parse(nowDate).getTime();
			}catch(Exception e){
				logger.error("Now Date "+nowDate+" is not parsing correctly. Needs to be in format yyyy-MM-dd HH:mm:ss ",e);
			}
		}
		return System.currentTimeMillis();
	}
}