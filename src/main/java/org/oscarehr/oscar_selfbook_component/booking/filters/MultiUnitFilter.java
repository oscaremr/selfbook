/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking.filters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class MultiUnitFilter  implements AvailableTimeSlotFilter{
	private static final Logger logger = MiscUtils.getLogger();
	
	@Override
	public List<TimeSlot> filterAvailableTimeSlots(Clinic clinic,String mrp,String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, List<TimeSlot> currentlyAllowedTimeSlots, Calendar date,Map<String,String> params){
		int timeSlotLen = dayWorkScheduleTransfer.getTimeSlotDurationMin();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS");
		// allowed time codes
		ArrayList<TimeSlot> filteredResults = new ArrayList<TimeSlot>();
		for(int i =0; i < currentlyAllowedTimeSlots.size();i++){
			TimeSlot entry = currentlyAllowedTimeSlots.get(i);
			Calendar apptStartTime = entry.getAvailableApptTime();
			Character code = entry.getCode();
			int apptLength = clinic.getAppointmentDuration(mrp, providerId, appointmentTypeId, code);
			
			if(apptLength>timeSlotLen){
				logger.debug("NEED TO CHECK THIS ONE "+apptLength+"  in a "+timeSlotLen);
				Calendar nextAppt = (Calendar) apptStartTime.clone();
				int apptBlockLen = timeSlotLen;
				for(int j = (i+1);j< currentlyAllowedTimeSlots.size();j++){
					apptBlockLen = apptBlockLen + timeSlotLen;
					logger.debug(j+":Big enough yet?? "+apptLength+"  in a "+apptBlockLen);
					
					TimeSlot nextTimeSlot = currentlyAllowedTimeSlots.get(j);
					nextAppt.add(Calendar.MINUTE, timeSlotLen);
					logger.debug("timeSlotStartTime " + sdf.format(nextAppt.getTime()) +"  nextAppt "+sdf.format(nextTimeSlot.getAvailableApptTime().getTime())+" == "+nextAppt.equals(nextTimeSlot.getAvailableApptTime()));
					if(nextAppt.equals(nextTimeSlot.getAvailableApptTime()) && (apptLength <= apptBlockLen)){
						filteredResults.add(entry);
						break;
					}
				}
				
				
			}else{
				filteredResults.add(entry);
			}
		}
		logger.debug("return "+filteredResults.size());
		return(filteredResults);
	}
	
	public List<TimeSlot> filterAvailableTimeSlots2(Clinic clinic,String mrp,String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, List<TimeSlot> currentlyAllowedTimeSlots, Calendar date,Map<String,String> params){
		int timeSlotLen = dayWorkScheduleTransfer.getTimeSlotDurationMin();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS");
		// allowed time codes
		ArrayList<TimeSlot> filteredResults = new ArrayList<TimeSlot>();
		for(int i =0; i < currentlyAllowedTimeSlots.size();i++){
			TimeSlot entry = currentlyAllowedTimeSlots.get(i);
			Calendar apptStartTime = entry.getAvailableApptTime();
			Character code = entry.getCode();
			int apptLength = clinic.getAppointmentDuration(mrp, providerId, appointmentTypeId, code);
			
			if(apptLength>timeSlotLen){
				if(i+1 <currentlyAllowedTimeSlots.size()){
					TimeSlot nextTimeSlot = currentlyAllowedTimeSlots.get(i+1);
					Calendar nextAppt = (Calendar) apptStartTime.clone();
					nextAppt.add(Calendar.MINUTE, timeSlotLen);
					
					logger.debug("timeSlotStartTime " + sdf.format(nextAppt.getTime()) +"  nextAppt "+sdf.format(nextTimeSlot.getAvailableApptTime().getTime())+" == "+nextAppt.equals(nextTimeSlot.getAvailableApptTime()));
					
					if(nextAppt.equals(nextTimeSlot.getAvailableApptTime())){
						filteredResults.add(entry);
					}
					nextTimeSlot.getCode();
				}
				
				logger.debug("NEED TO CHECK THIS ONE "+apptLength+"  in a "+timeSlotLen);
				
			}
		}
		logger.debug("return "+filteredResults.size());
		return(filteredResults);
	}
}
