/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.booking.filters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.oscar_selfbook_component.manager.BookingLearningManager;
import org.oscarehr.oscar_selfbook_component.manager.OscarScheduleManager;
import org.oscarehr.ws.AppointmentTransfer;
import org.oscarehr.ws.DayWorkScheduleTransfer;

public class ExistingAppointmentFilter implements AvailableTimeSlotFilter{
	private static final Logger logger = MiscUtils.getLogger();
	@Override
	public List<TimeSlot> filterAvailableTimeSlots(Clinic clinic,String mrp,String providerId, Long appointmentTypeId, DayWorkScheduleTransfer dayWorkScheduleTransfer, List<TimeSlot> currentlyAllowedTimeSlots, Calendar date,Map<String,String> params){
		ArrayList<TimeSlot> allowedTimesFilteredByExistingAppointments = new ArrayList<TimeSlot>();
                long requireTimeMillisSeconds;
		try{
			List<AppointmentTransfer> existingAppointments = OscarScheduleManager.getAppointments(providerId, date);
			for (TimeSlot startTime : currentlyAllowedTimeSlots){
                            requireTimeMillisSeconds  = (clinic.getAppointmentDuration(mrp, providerId, appointmentTypeId, startTime.getCode()) *60 *1000);
				if (!isThisTakenByExistingAppointment(startTime, requireTimeMillisSeconds, existingAppointments)){
					allowedTimesFilteredByExistingAppointments.add(startTime);
				}
			}
		}catch (Exception e){
			logger.error("Error getting existing appointments provider:"+providerId,e);
		}
		if(allowedTimesFilteredByExistingAppointments.size() == 0){
			BookingLearningManager.recommendDayToBeSkipped(clinic, providerId, date, appointmentTypeId, this.getClass().getName());
		}
		
		return allowedTimesFilteredByExistingAppointments;

	}

	private static boolean isThisTakenByExistingAppointment(TimeSlot timeSlotStartTime, long requireTimeMillisSeconds, List<AppointmentTransfer> existingAppointments){
		long timeSlotStartTimeMs = timeSlotStartTime.getAvailableApptTime().getTimeInMillis();
                
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS");
		// check if there's already an appointment in this time
		for (AppointmentTransfer appointment : existingAppointments){
			if (appointment.getAppointmentStartDateTime() == null || appointment.getAppointmentEndDateTime() == null){
				//logger.error("Appointment is missing timeframe, can't sort out availability. " + getDebugString(appointment));
				return(true);
			}

			long appointmentStart = appointment.getAppointmentStartDateTime().getTimeInMillis();
			long appointmentEnd = appointment.getAppointmentEndDateTime().getTimeInMillis();
			boolean collide = false;

			// cases
			// -------------------------------------------------
			//             |--EXISINTG_APPOINTMENT--|
			// good    |
			// bad       |  
			// bad         | 
			// bad                   |
			// bad                                |
			// good                                 |
			// bad    |----------------------------------|

			// if this timeslot ends before the appointment starts we're ok
			if (timeSlotStartTimeMs >= appointmentStart && timeSlotStartTimeMs <= appointmentEnd) collide = true;
                        if ((timeSlotStartTimeMs+requireTimeMillisSeconds) > appointmentStart && timeSlotStartTimeMs < appointmentStart) collide = true;
			logger.debug("timeSlotStartTime " + sdf.format(timeSlotStartTime.getAvailableApptTime().getTime()) + " appointmentStart " + sdf.format(appointment.getAppointmentStartDateTime().getTime()) + " appointmentEnd " + sdf.format(appointment.getAppointmentEndDateTime().getTime()) + " collide " + collide);
			if (collide) return(true);
		}
		return(false);
	}
}
