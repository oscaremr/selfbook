/**
* Copyright (c) 2016 - selfbook.ca 
*
* This software is published under the GPL GNU General Public License.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version. 
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*
* This software was written by Tan Pham for
* selfbook.ca, Saint-Laurent, Quebec, Canada to be provided
* as part of the OSCAR McMaster EMR System
**/

package org.oscarehr.oscar_selfbook_component.booking;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.XmlUtils;
import org.w3c.dom.Node;

public class Algorithm {

    private static final Logger logger = MiscUtils.getLogger();
    String providerNo = null;
    Map<String, Integer> answerRadioDisplayText = null;
    Map<String, Integer> answerNextQuestion = null;
    Map<String, Integer> answerDateBeforeDays = null;

    String questionNo = null;
    String nextQuestionNo = null;
    String subQuestionNo = null;
    String displayText = null;
    String answerType = null;
    String validation = null;
    String apptType = null;
    String apptDesc = null;

    public String getProviderNo() {
        return providerNo;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public String getNextQuestionNo() {
        return nextQuestionNo;
    }

    public String getSubQuestionNo() {
        return subQuestionNo;
    }

    public String getDisplayText() {
        return displayText;
    }

    public String getAnswerType() {
        return answerType;
    }

    public String getApptType() {
        return apptType;
    }

    public String getApptDesc() {
        return apptDesc;
    }

    public String getValidation() {
        return validation;
    }

    public Map<String, Integer> getAnswerRadioDisplayTexts() {
        return answerRadioDisplayText;
    }

    public Map<String, Integer> getAnswerDateBeforeDays() {
        return answerDateBeforeDays;
    }
   
    public static Algorithm fromXml(Node node) {
        Algorithm algorithm = new Algorithm();
        algorithm.questionNo = XmlUtils.getAttributeValue(node, "questionNo");
        algorithm.nextQuestionNo = XmlUtils.getAttributeValue(node, "nextQuestionNo");
        algorithm.subQuestionNo = XmlUtils.getAttributeValue(node, "subQuestionNo");
        algorithm.displayText = XmlUtils.getAttributeValue(node, "displayText");
        algorithm.answerType = XmlUtils.getAttributeValue(node, "answerType");
        algorithm.validation = XmlUtils.getAttributeValue(node, "validation");
        algorithm.apptType = XmlUtils.getAttributeValue(node, "apptType");
        algorithm.apptDesc = XmlUtils.getAttributeValue(node, "apptDesc");
        List<Node> answerRadioNodes = XmlUtils.getChildNodes(node, "answerRadio");
        algorithm.answerRadioDisplayText = getAnswerRadioDisplayText(answerRadioNodes);
        List<Node> answerDateNodes = XmlUtils.getChildNodes(node, "answerDate");
        algorithm.answerDateBeforeDays = getAnswerDates(answerDateNodes);

        return algorithm;
    }

    private static Map<String, Integer> getAnswerRadioDisplayText(List<Node> answerRadioNodes) {
        Map<String, Integer> map = new LinkedHashMap<String, Integer>();
        for (Node answerRadioNode : answerRadioNodes) {
            String displayText = null;
            String nextQuestionNo = null;
            try {
                displayText = XmlUtils.getAttributeValue(answerRadioNode, "displayText");
                nextQuestionNo = XmlUtils.getAttributeValue(answerRadioNode, "nextQuestionNo");
                if (displayText != null && nextQuestionNo != null) {
                    map.put(displayText, Integer.parseInt(nextQuestionNo));
                }
            } catch (Exception e) {
                logger.debug("Not a Integer  nextQuestionNo " + nextQuestionNo, e);
            }
        }
        return map;
    }

    private static Map<String, Integer> getAnswerDates(List<Node> answerDateNodes) {
        Map<String, Integer> map = new LinkedHashMap<String, Integer>();
        for(Node answerDateNode : answerDateNodes) {
            String days = null;
            String nextQuestionNo = null;
            try {
                days = XmlUtils.getAttributeValue(answerDateNode, "beforedays");
                nextQuestionNo = XmlUtils.getAttributeValue(answerDateNode, "nextQuestionNo");
                if (days != null && nextQuestionNo != null) {
                    map.put(days, Integer.parseInt(nextQuestionNo));
                }
            } catch (Exception e) {
                logger.debug("Not a Integer  nextQuestionNo " + nextQuestionNo, e);
            }
        }
        return map;
    }

}
