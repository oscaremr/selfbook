/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.oscarehr.util.CxfClientUtils;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.ws.DemographicWs;
import org.oscarehr.ws.DemographicWsService;
import org.oscarehr.ws.FacilityWs;
import org.oscarehr.ws.FacilityWsService;
import org.oscarehr.ws.LoginResultTransfer;
import org.oscarehr.ws.LoginWs;
import org.oscarehr.ws.LoginWsService;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderWs;
import org.oscarehr.ws.ProviderWsService;
import org.oscarehr.ws.ScheduleWs;
import org.oscarehr.ws.ScheduleWsService;
import org.oscarehr.ws.SystemInfoWs;
import org.oscarehr.ws.SystemInfoWsService;

public class OscarServerWebServicesManager{
	private static Logger logger = MiscUtils.getLogger();
	private static String oscarServerBaseUrl = ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "oscar_server_base_url");
	public static String oscarServerUserName =  ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "oscar_server_user");
	private static String oscarServerPassword = ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "oscar_server_password");
	private static LoginResultTransfer loginResultTransfer = null;
        private static String disableHostnameVerifier = ConfigXmlUtils.getPropertyString("misc", "disable_hostname_verifier");

      	static
	{
		CxfClientUtils.initSslFromConfig();
	}

	public static void initLoginResultTransfer() throws NotAuthorisedException_Exception{
                if("true".equals(disableHostnameVerifier)) {
                    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                    new javax.net.ssl.HostnameVerifier(){
                        public boolean verify(String hostname,
                        javax.net.ssl.SSLSession sslSession) {
                            return true;
                          }
                    });
                }

		LoginWs loginWs = getLoginWs();
		LoginResultTransfer temp = loginWs.login(oscarServerUserName, oscarServerPassword);
		if (temp != null){
			loginResultTransfer = temp;
		}
	}

	public static String getMyoscarServerBaseUrl(){
		return(oscarServerBaseUrl);
	}

	private static URL buildURL(String servicePoint){
		String urlString = oscarServerBaseUrl + '/' + servicePoint + "?wsdl";

		logger.debug(urlString);

		try{
			return(new URL(urlString));
		}catch (MalformedURLException e){
			logger.error("Invalid Url : " + urlString, e);
			return(null);
		}
	}

	public static SystemInfoWs getSystemPropertiesWs(){
		SystemInfoWsService service = new SystemInfoWsService(buildURL("SystemInfoService"));
		SystemInfoWs port = service.getSystemInfoWsPort();

		CxfClientUtils.configureClientConnection(port);

		return(port);
	}

	public static LoginWs getLoginWs(){
		LoginWsService service = new LoginWsService(buildURL("LoginService"));
		LoginWs port = service.getLoginWsPort();

		CxfClientUtils.configureClientConnection(port);

		return(port);
	}

	public static ScheduleWs getScheduleWs() throws NotAuthorisedException_Exception{
		if (loginResultTransfer == null) initLoginResultTransfer();

		ScheduleWsService service = new ScheduleWsService(buildURL("ScheduleService"));
		ScheduleWs port = service.getScheduleWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}

	public static ProviderWs getProviderWs() throws NotAuthorisedException_Exception{
		if (loginResultTransfer == null) initLoginResultTransfer();

		ProviderWsService service = new ProviderWsService(buildURL("ProviderService"));
		ProviderWs port = service.getProviderWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}

	public static DemographicWs getDemographicWs() throws NotAuthorisedException_Exception{
		if (loginResultTransfer == null) initLoginResultTransfer();

		DemographicWsService service = new DemographicWsService(buildURL("DemographicService"));
		DemographicWs port = service.getDemographicWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}

	public static FacilityWs getFacilityWs() throws NotAuthorisedException_Exception{
		if (loginResultTransfer == null) initLoginResultTransfer();

		FacilityWsService service = new FacilityWsService(buildURL("FacilityService"));
		FacilityWs port = service.getFacilityWsPort();

		CxfClientUtils.configureClientConnection(port);
		CxfClientUtils.addWSS4JAuthentication(loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey(), port);

		return(port);
	}
}