/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University.
 * All Rights Reserved. This software is published under the GPL GNU General
 * Public License. This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the Department of Family Medicine McMaster
 * University Hamilton Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.utils;

import org.apache.commons.codec.binary.Base64;
import org.oscarehr.util.ConfigXmlUtils;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

public final class EncryptionUtils {

    final String password = ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "encryption_password");
    final String salt = ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "encryption_salt");

    public String encrypt(String toEncrypt) throws Exception {
        TextEncryptor encryptor = Encryptors.text(password, salt);
        String encryptedText = encryptor.encrypt(toEncrypt);
        byte[] encryptedByteArray = encryptedText.getBytes("UTF8");
        byte[] encodedBytes = Base64.encodeBase64(encryptedByteArray);
        return new String(encodedBytes);

    }

    public String decrypt(String toDecrypt) throws Exception {
        TextEncryptor decryptor = Encryptors.text(password, salt);
        byte[] encryptedData = Base64.decodeBase64(toDecrypt.getBytes("UTF8"));
        String decryptedText = decryptor.decrypt(new String(encryptedData));
        return decryptedText;

    }

}
