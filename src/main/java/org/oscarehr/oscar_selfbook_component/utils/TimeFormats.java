/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.apache.commons.lang.time.DateFormatUtils;

public class TimeFormats{

	public static String getTimeDisplayHtmlEscaped(Locale locale, Calendar date){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", locale);
		return(simpleDateFormat.format(date.getTime()));
	}
	
	public static String getWeekDay(Locale locale, Calendar date){
		SimpleDateFormat justWeekDay = new SimpleDateFormat("EEEE", locale);
		return justWeekDay.format(date.getTime());
	}
	
	public static String getDayMonthYearExt(Locale locale, Date date){
		SimpleDateFormat extendDate = new SimpleDateFormat("dd MMMM, yyyy", locale);
		return extendDate.format(date);
	}
	
	public static String getDayMonthYearExt(Locale locale, Calendar date){
		return getDayMonthYearExt(locale,date.getTime());
	}
		
	public static String getFullDateTime(Locale locale, Calendar date){
		SimpleDateFormat extendDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", locale);
		return extendDate.format(date.getTime());
	}
	
	public static String getLongExtDateTime(Locale locale,Calendar date){
		SimpleDateFormat extendDate = new SimpleDateFormat("EEEE, yyyy-MM-dd, hh:mm a", locale);
		return extendDate.format(date.getTime());
	}
	
	public static String getISODateFormat(Locale locale, Calendar date){
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormatUtils.ISO_DATE_FORMAT.getPattern());
		return sdf.format(date.getTime());
	}
	
	public static String getJustDateFormat(Locale locale, Calendar date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date.getTime());
	}
	
	public static Calendar parseJustDateFormat(String date) throws java.text.ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(date));
		return  cal;
	}
	
	
	
}