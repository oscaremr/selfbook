/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.utils;

import com.sun.mail.util.MailSSLSocketFactory;
import org.oscarehr.util.MiscUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import org.oscarehr.util.ConfigXmlUtils;

public final class EmailUtils {

    private static Logger logger = MiscUtils.getLogger();
    private static final String CATEGORY = "smtp";
    public static String smtpHost = ConfigXmlUtils.getPropertyString(CATEGORY, "host");
    public static String smtpSslPort = ConfigXmlUtils.getPropertyString(CATEGORY, "ssl_port");
    public static String smtpUser = ConfigXmlUtils.getPropertyString(CATEGORY, "user");
    public static String smtpPassword = ConfigXmlUtils.getPropertyString(CATEGORY, "password");
    public static String smtpTimeout = ConfigXmlUtils.getPropertyString(CATEGORY, "smtpTimeout");
    public static String recipientOverride = ConfigXmlUtils.getPropertyString(CATEGORY, "recipient_override");
    public static String smtpConnectionSecurity = ConfigXmlUtils.getPropertyString(CATEGORY, "connection_security");
    public static String printInsteadOfSend = ConfigXmlUtils.getPropertyString(CATEGORY, "print_instead_of_send");
    public static String trustAllHosts = ConfigXmlUtils.getPropertyString(CATEGORY, "trustAllHosts");

    private static final String CONNECTION_SECURITY_SSL = "ssl";
    private static final String CONNECTION_SECURITY_STARTTLS = "starttls";

    private static class HtmlEmailWrapper extends HtmlEmail {

        @Override
        public String sendMimeMessage() {
            try {
                if (recipientOverride != null) {
                    message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientOverride));
                    message.setRecipients(Message.RecipientType.CC, new InternetAddress[0]);
                    message.setRecipients(Message.RecipientType.BCC, new InternetAddress[0]);
                }
                if ("true".equals(printInsteadOfSend)) {
                    logger.info(getAsString(message));
                } else {
                    logger.debug("Sending Email to " + Arrays.toString(message.getAllRecipients()));
                    return (super.sendMimeMessage());
                }
            } catch (Exception e) {
                logger.error("Error", e);
            }
            return (null);
        }
    }

    public static HtmlEmail getHtmlEmail(String smtpServer, String smtpPort, final String smtpUser, final String smtpPassword, String connectionSecurity) throws EmailException {
        logger.debug("smtpServer=" + smtpServer + ", smtpSslPort=" + smtpPort + ", smtpUser=" + smtpUser + ", smtpPassword=" + smtpPassword + ",connectionSecurity=" + connectionSecurity);

        HtmlEmail email = null;

        if (recipientOverride != null || "true".equals(printInsteadOfSend)) {
            email = new HtmlEmailWrapper();
        } else {
            email = new HtmlEmail();
        }
        //email.setDebug(true);

        email.setHostName(smtpServer);
        email.setCharset("utf-8");
        if (smtpUser != null && smtpPassword != null) {
            email.setAuthentication(smtpUser, smtpPassword);

        }
        if (connectionSecurity != null) {
            if (connectionSecurity.equals(CONNECTION_SECURITY_STARTTLS)) {
                email.setStartTLSEnabled(true);
            } else if (connectionSecurity.equals(CONNECTION_SECURITY_SSL)) {
                email.setSSLOnConnect(true);
            }
        }

        if (smtpPort != null) {
            email.setSslSmtpPort(smtpPort);
            email.setSmtpPort(Integer.parseInt(smtpPort));

        }
        Session session = email.getMailSession();
        Properties properties = session.getProperties();
//        properties.setProperty("mail.smtp.port", smtpPort);

        MailSSLSocketFactory socketFactory;
        try {
            socketFactory = new MailSSLSocketFactory();
            socketFactory.setTrustAllHosts(true);
            if ("true".equals(trustAllHosts)) {
                properties.put("mail.smtp.socketFactory", socketFactory);
            }
        } catch (GeneralSecurityException ex) {
            java.util.logging.Logger.getLogger(EmailUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        properties.setProperty("mail.smtp.connectiontimeout", "20000");
        properties.setProperty("mail.smtp.timeout", "20000");
        return (email);
    }

    /**
     * This method will return an HtmlEmail object populated with the
     * smtpServer/smtpUser/smtpPassword from the config xml file.
     *
     * @throws EmailException
     */
    public static HtmlEmail getHtmlEmail() throws EmailException {
        return (getHtmlEmail(smtpHost, smtpSslPort, smtpUser, smtpPassword, smtpConnectionSecurity));
    }

    /**
     * This is a convenience method for sending and email to 1 recipient using
     * the configuration file settings.
     *
     * @throws EmailException
     */
    public static String sendEmail(String toEmailAddress, String toName, String fromEmailAddress, String fromName, String toBccEmailAddress, String subject, String textContents, String htmlContents) throws EmailException {
        HtmlEmail htmlEmail = getHtmlEmail();
        htmlEmail.addTo(toEmailAddress, toName);
        if (toBccEmailAddress != null) {
            htmlEmail.addBcc(toBccEmailAddress);
        }
        if (fromEmailAddress == null) {
            fromEmailAddress = smtpUser;
        }
        htmlEmail.setFrom(fromEmailAddress, fromName);
        htmlEmail.setSubject(subject);
        if (textContents != null) {
            htmlEmail.setTextMsg(textContents);
        }
        if (htmlContents != null) {
            htmlEmail.setHtmlMsg(htmlContents);
        }
        return (htmlEmail.send());
    }

    /**
     * This method is like a toString for Email objects.
     */
    public static String getAsString(Email email) throws IOException, MessagingException, EmailException {
        email.buildMimeMessage();
        return (getAsString(email.getMimeMessage()));
    }

    /**
     * This method is like a toString for Email objects.
     */
    public static String getAsString(MimeMessage mimeMessage) throws IOException, MessagingException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        mimeMessage.writeTo(os);
        return (os.toString());
    }
}
