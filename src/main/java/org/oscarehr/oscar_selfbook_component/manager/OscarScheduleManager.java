/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.manager;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.utils.BenchmarkTimer;
import org.oscarehr.oscar_selfbook_component.utils.OscarServerWebServicesManager;
import org.oscarehr.oscar_selfbook_component.utils.TimeFormats;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.AppointmentTransfer;
import org.oscarehr.ws.BookingSource;
import org.oscarehr.ws.DayWorkScheduleTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ScheduleWs;

public final class OscarScheduleManager{
	private static Logger logger=MiscUtils.getLogger();
	private static final int MAX_OBJECTS_TO_CACHE = 1024;
	private static final long MAX_TIME_TO_CACHE = DateUtils.MILLIS_PER_HOUR;

	private static QueueCache<String,DayWorkScheduleTransfer> dayWorkScheduleTransferCache = new QueueCache<String,DayWorkScheduleTransfer>("OSM",4, MAX_OBJECTS_TO_CACHE, MAX_TIME_TO_CACHE,null);
	
	public static List<AppointmentTransfer> getAppointments(String providerNo, Calendar date) throws NotAuthorisedException_Exception{
		BenchmarkTimer btimer = new BenchmarkTimer("getApptsFromOSCAR "+providerNo+" "+date.getTime());
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		btimer.tag("gotSchedule");
		List<AppointmentTransfer> appointmentTransfers = scheduleWs.getAppointmentsForProvider(providerNo, date);
		btimer.tag("getAppts");
		logger.debug(btimer.report());
		return(appointmentTransfers);
	}

	public static DayWorkScheduleTransfer getDayWorkSchedule(String providerNo, Calendar date) throws NotAuthorisedException_Exception{
		String key = providerNo+""+TimeFormats.getJustDateFormat(Locale.CANADA, date);
		
		DayWorkScheduleTransfer dayWorkScheduleTransfer  = dayWorkScheduleTransferCache.get(key);
		if(dayWorkScheduleTransfer ==null){
			logger.debug(key+ " not found in hash "+dayWorkScheduleTransferCache.size());
			ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
			dayWorkScheduleTransfer = scheduleWs.getDayWorkSchedule(providerNo, date);
			if(dayWorkScheduleTransfer == null){ //HACK:  but it keeps calling the webservice again it's null
				dayWorkScheduleTransfer = new DayWorkScheduleTransfer();
				dayWorkScheduleTransfer.setHoliday(Boolean.TRUE);
			}
			dayWorkScheduleTransferCache.put(key, dayWorkScheduleTransfer); 
		}
		return(dayWorkScheduleTransfer);
	}

	public static Integer addAppointment(Calendar startTime, Calendar endTime, Integer demographicId, String name, String notes, String providerNo,String reason) throws NotAuthorisedException_Exception{
		AppointmentTransfer appointment = new AppointmentTransfer();
		appointment.setAppointmentEndDateTime(endTime);
		appointment.setAppointmentStartDateTime(startTime);
		appointment.setDemographicNo(demographicId);
		appointment.setName(name);
		appointment.setNotes(notes);
		appointment.setReason(reason);
		appointment.setProviderNo(providerNo);
		appointment.setStatus("t"); // dunno what that means but seems to be what everyone else uses
		appointment.setBookingSource(BookingSource.MYOSCAR_SELF_BOOKING);

		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		Integer result = scheduleWs.addAppointment(appointment);
		return(result);
	}
        	public static Integer addAppointment(Calendar startTime, Calendar endTime, Integer demographicId, String name, String notes, String providerNo,String reason,String phone,String location, String email) throws NotAuthorisedException_Exception{
		AppointmentTransfer appointment = new AppointmentTransfer();
		appointment.setAppointmentEndDateTime(endTime);
		appointment.setAppointmentStartDateTime(startTime);
		appointment.setDemographicNo(demographicId);
		appointment.setName(name);
		appointment.setNotes(phone+" "+notes);
		appointment.setReason(reason);
		appointment.setProviderNo(providerNo);
		appointment.setStatus("t"); // dunno what that means but seems to be what everyone else uses
		appointment.setBookingSource(BookingSource.MYOSCAR_SELF_BOOKING);
                appointment.setLocation(location);
                appointment.setType("");
                appointment.setResources(email);
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		Integer result = scheduleWs.addAppointment(appointment);
		return(result);
	}

	public static List<AppointmentTransfer> getAppointmentsForPatient(Integer demographicId, int startIndex, int itemsToReturn) throws NotAuthorisedException_Exception{
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		List<AppointmentTransfer> appointmentTransfers = scheduleWs.getAppointmentsForPatient(demographicId, startIndex, itemsToReturn);
		return(appointmentTransfers);
	}

	public static void updateAppointment(AppointmentTransfer theAppointment) throws NotAuthorisedException_Exception{
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		scheduleWs.updateAppointment(theAppointment);
	}
        
        public static void updateAppointmentStatus(Integer apptId, String apptStatus) throws NotAuthorisedException_Exception{
		ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		AppointmentTransfer theAppointment=scheduleWs.getAppointment(apptId);
                theAppointment.setStatus(apptStatus);
                scheduleWs.updateAppointment(theAppointment);
	}
        
        public AppointmentTransfer getAppointment(Integer apptId) throws NotAuthorisedException_Exception{
                ScheduleWs scheduleWs = OscarServerWebServicesManager.getScheduleWs();
		AppointmentTransfer theAppointment=scheduleWs.getAppointment(apptId);
                return(theAppointment);
        }
}
