/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.manager;

import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.oscar_selfbook_component.utils.OscarServerWebServicesManager;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;
import org.oscarehr.ws.ProviderWs;

public final class OscarProviderManager{
	private static final int MAX_OBJECTS_TO_CACHE = 4;
	private static final long MAX_TIME_TO_CACHE = DateUtils.MILLIS_PER_HOUR;

	/**
	 * key is true=active | false=inactive providers list
	 */
	private static QueueCache<Boolean, List<ProviderTransfer>> providerCache = new QueueCache<Boolean, List<ProviderTransfer>>("OPM", 4, MAX_OBJECTS_TO_CACHE, MAX_TIME_TO_CACHE,null);

	public static List<ProviderTransfer> getProviders(boolean active) throws NotAuthorisedException_Exception{
		List<ProviderTransfer> results = providerCache.get(active);

		if (results == null){
			ProviderWs providerWs = OscarServerWebServicesManager.getProviderWs();
			results = providerWs.getProviders(active);
			providerCache.put(active, results);
		}

		return(results);
	}

	public static ProviderTransfer getProvider(String providerId) throws NotAuthorisedException_Exception{
		// check active providers
		List<ProviderTransfer> results = getProviders(true);
		for (ProviderTransfer providerTransfer : results){
			if (providerTransfer.getProviderNo().equals(providerId)) return(providerTransfer);
		}

		// check inactive providers
		results = getProviders(false);
		for (ProviderTransfer providerTransfer : results){
			if (providerTransfer.getProviderNo().equals(providerId)) return(providerTransfer);
		}

		return(null);
	}
}
