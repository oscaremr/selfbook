/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.manager;

import java.util.List;
import org.oscarehr.oscar_selfbook_component.utils.OscarServerWebServicesManager;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.DemographicWs;
import org.oscarehr.ws.NotAuthorisedException_Exception;

public final class OscarDemographicManager{
	public static DemographicTransfer getDemographicByMyOscarUserName(String myOscarUserName) throws NotAuthorisedException_Exception{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs();
		DemographicTransfer result = demographicWs.getDemographicByMyOscarUserName(myOscarUserName);
		return(result);
	}
	public static DemographicTransfer getDemographicByDemographicNo(int demographicNo) throws NotAuthorisedException_Exception{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs();
		DemographicTransfer result = demographicWs.getDemographic(demographicNo);
		return(result);
	}    
      	public static DemographicTransfer getDemographicByNam(String nam) throws NotAuthorisedException_Exception{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs();
		List<DemographicTransfer> result = demographicWs.searchDemographicsByAttributes(nam, null,null,null,null,null,null,null,null,null,0,1);
                if(result!=null && result.size()==1) {
                 return result.get(0);
                } else {
                    return null;		
                }
	}
}
