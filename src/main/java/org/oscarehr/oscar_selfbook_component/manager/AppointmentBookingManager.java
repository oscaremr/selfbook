/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.manager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.FilterDefinition;
import org.oscarehr.oscar_selfbook_component.booking.Provider;
import org.oscarehr.oscar_selfbook_component.booking.TimeSlot;
import org.oscarehr.oscar_selfbook_component.booking.filters.AvailableTimeSlotFilter;
import org.oscarehr.oscar_selfbook_component.utils.BenchmarkTimer;
import org.oscarehr.oscar_selfbook_component.utils.TimeFormats;
import org.oscarehr.util.XmlUtils;
import org.oscarehr.ws.CalendarScheduleCodePairTransfer;
import org.oscarehr.ws.DayWorkScheduleTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AppointmentBookingManager {
	private static final Logger logger = MiscUtils.getLogger();
	
	public static List<TimeSlot> findAppointments(Clinic clinic,String providerAlias,Long appointmentTypeId,Calendar startDate) throws NotAuthorisedException_Exception,java.lang.ClassNotFoundException,java.lang.InstantiationException,java.lang.IllegalAccessException{
		return findAppointments(clinic,providerAlias,appointmentTypeId,startDate,null);
	}
	
	/*
	DaysToSearchAheadLimit = Number of days it searches before giving up. ie search for the next 60 days before giving up
	NumberOfAppointmentOptionsToReturn = Number of appts that seems like it gives a reasonable choice.
	 ========================
		get Circle of Care Providers capable of dealing with appt Type
		for each provider retrieve their DayWorkSchedule
		
		//check if it's a holiday
		//get providers timecodes for appt Type
		//how to eliminate 30 min codes represented as 2 15min segments eg 1112211___ codes really should just be eg 1112_11___
		//Then filter for future	
	*/
	public static List<TimeSlot> findAppointments(Clinic clinic,String providerAlias,Long appointmentTypeId,Calendar startDate,Document doc) throws NotAuthorisedException_Exception,java.lang.ClassNotFoundException,java.lang.InstantiationException,java.lang.IllegalAccessException{
		//Which providers can handle that appt type?
		BenchmarkTimer btimer = new BenchmarkTimer("findAppt");
		Map<Provider, Character[]> providerMap = clinic.getProvidersForAppointmentType(providerAlias, appointmentTypeId);
		List<TimeSlot> appointments = new ArrayList<TimeSlot>();
		btimer.tag("after getProviders");
		for(int i=0;i<clinic.getDaysToSearchAheadLimit();i++){
			Calendar calDayToSearch = (Calendar) startDate.clone();
			calDayToSearch.add(Calendar.DAY_OF_YEAR, i);
			
			Element searchRecord = recordDateSearched(doc, calDayToSearch);
			
			for(Provider provider : providerMap.keySet()){
				//List<TimeSlot> providerAppointments = new ArrayList<TimeSlot>();
				DayWorkScheduleTransfer dayWorkScheduleTransfer = OscarScheduleManager.getDayWorkSchedule(provider.getProviderNo(), calDayToSearch);
				btimer.tag("get schedule for "+provider.getProviderNo());
				Element searchedProviderRecord = recordProviderSearched(doc,searchRecord,provider.getProviderNo(),dayWorkScheduleTransfer, providerMap.get(provider));
				btimer.tag("record for "+provider.getProviderNo());
				if (dayWorkScheduleTransfer == null || dayWorkScheduleTransfer.isHoliday())	continue;
				if (BookingLearningManager.isDaySetToSkip(clinic, provider.getProviderNo(), calDayToSearch, appointmentTypeId)) continue;
				
				List<TimeSlot> providerAppointments = Clinic.getAllowedTimesByType(dayWorkScheduleTransfer, providerMap.get(provider),provider.getProviderNo());
				recordFilterForSearchedProvider(doc,searchedProviderRecord,dayWorkScheduleTransfer,"N/A" , providerAppointments);						
				
				
				List<FilterDefinition> filterClassNames = provider.getFilter();
				if (filterClassNames != null){
					for (FilterDefinition className : filterClassNames){
						btimer.tag("start "+className.getFilterClassName());
						@SuppressWarnings("unchecked")
						Class<AvailableTimeSlotFilter> filterClass = (Class<AvailableTimeSlotFilter>) Class.forName(className.getFilterClassName());
						logger.debug("filter class null? "+filterClass.getName());
						AvailableTimeSlotFilter filterClassInstance = filterClass.newInstance();
						providerAppointments = filterClassInstance.filterAvailableTimeSlots(clinic,provider.getProviderNo(),provider.getProviderNo(), appointmentTypeId, dayWorkScheduleTransfer, providerAppointments,calDayToSearch,className.getParams());
						recordFilterForSearchedProvider(doc,searchedProviderRecord,dayWorkScheduleTransfer,filterClassInstance.getClass().getSimpleName() , providerAppointments);						
						btimer.tag("end "+className.getFilterClassName());
						if(providerAppointments.size() == 0){ //No need to continue filtering if there's nothing left to filter
							break;
						}
					}
				}		
				appointments.addAll(providerAppointments);
			}
			if (appointments.size() > clinic.getNumberOfAppointmentOptionsToReturn()){
				break; //Should return the date here so that it's known what date to search from next.
			}
		}
		btimer.tag("sorting");
		Collections.sort(appointments,TimeSlot.getTimeSlotComparator());
		logger.debug(btimer.report(true));
		return appointments;
	}
	
	public static Document getSearchRecordDocument() throws ParserConfigurationException{
		return XmlUtils.newDocument("search");
	}
	
	private static Element recordFilterForSearchedProvider(Document doc, Element searchedProviderRecord,DayWorkScheduleTransfer dayWorkScheduleTransfer,String filter, List<TimeSlot> providerAppointments){
		logger.debug("filter name "+filter);
		Element child = null;
		if (doc != null){
			child = doc.createElement("filter");
			child.setAttribute("name",filter);
			child.setAttribute("size",""+providerAppointments.size());
			
			for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots()){
				Integer i = entry.getScheduleCode();
				char c = (char) (i.intValue());
			
				String timeSlotAvailable = "false";
				for(TimeSlot ts: providerAppointments){
					if(entry.getDate().equals(ts.getAvailableApptTime())){
						timeSlotAvailable = "success";  	
					}
				}
			
				Element timeslotChild = doc.createElement("timeslot");
				timeslotChild.setAttribute("available", timeSlotAvailable);
				timeslotChild.setAttribute("time", TimeFormats.getTimeDisplayHtmlEscaped(Locale.CANADA, entry.getDate()));
				timeslotChild.setAttribute("code",""+c);
				child.appendChild(timeslotChild);
			}
				
			searchedProviderRecord.appendChild(child);
		}
		return child;
	}

	private static Element recordProviderSearched(Document doc,Element searchRecord,String providerNo, DayWorkScheduleTransfer dayWorkScheduleTransfer, Character[] codes){
		logger.debug("does provider:"+providerNo+" have a day schedule? "+dayWorkScheduleTransfer+ " and codes "+codes);
		Element child = null;
		if (doc != null){
			child = doc.createElement("searchedprovider");
			child.setAttribute("providerNo", providerNo);
			child.setAttribute("dayWorkScheduleTransfer", getScheduleString(dayWorkScheduleTransfer));
			child.setAttribute("codes", getCharCodes(codes));
			child.setAttribute("holiday:",""+dayWorkScheduleTransfer.isHoliday());
			searchRecord.appendChild(child);
		}
		return child;
	}
	
	private static Element recordDateSearched(Document doc,Calendar calDayToSearch){
		logger.debug("searching this date :"+TimeFormats.getFullDateTime(Locale.CANADA,calDayToSearch));
		Element child = null;
		if (doc !=null){
			child = doc.createElement("searcheddate");
			child.setAttribute("date",TimeFormats.getFullDateTime(Locale.CANADA,calDayToSearch));
			doc.getDocumentElement().appendChild(child);
		}
		return child;
	}
	
	private static  String getScheduleString(DayWorkScheduleTransfer dayWorkScheduleTransfer){
		if(dayWorkScheduleTransfer == null){
			return "N/A";
		}
		StringBuilder sb = new StringBuilder();	
		for (CalendarScheduleCodePairTransfer entry : dayWorkScheduleTransfer.getTimeSlots()){
			Integer i = entry.getScheduleCode();
			char c = (char) (i.intValue());
			sb.append(c);
		}
		return sb.toString();
	}

	private static String getCharCodes(Character[] chars){
		StringBuilder sb = new StringBuilder();
		for(Character c:chars){
			sb.append(c);
			sb.append(",");
		}
		return sb.toString();
	}
	

}
