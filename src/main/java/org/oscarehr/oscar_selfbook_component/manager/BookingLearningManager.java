/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.manager;

import java.util.Calendar;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.util.QueueCache;

public class BookingLearningManager {
	private static Logger logger=MiscUtils.getLogger();
	private static final int MAX_OBJECTS_TO_CACHE = 1024;
	private static final long MAX_TIME_TO_CACHE = DateUtils.MILLIS_PER_HOUR;

	private static QueueCache<String,String> bookingLearningCache = new QueueCache<String,String>("BLM",4, MAX_OBJECTS_TO_CACHE, MAX_TIME_TO_CACHE,null);

	
	public static Boolean isDaySetToSkip(Clinic clinic,String providerNo, Calendar day, Long appointmentTypeId){
		if(bookingLearningCache.get(getKey(clinic,providerNo,day,appointmentTypeId)) != null){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public static void recommendDayToBeSkipped(Clinic clinic,String providerNo, Calendar day, Long appointmentTypeId,String reason){
		logger.debug("clinic"+clinic.getName()+" provider No "+providerNo +" "+day.getTime()+" " + appointmentTypeId+" "+reason);
		bookingLearningCache.put(getKey(clinic,providerNo,day,appointmentTypeId), reason);
	}
	
	private static String getKey(Clinic clinic,String providerNo, Calendar day, Long appointmentTypeId){
		return clinic.getName()+":"+providerNo+":"+day.getTimeInMillis()+":"+appointmentTypeId;
	}
	
}
