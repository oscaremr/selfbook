/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.time.DateFormatUtils;
import org.oscarehr.oscar_selfbook_component.manager.OscarProviderManager;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;

public final class ViewWeekAppointmentSlots{
	public static final int DAYS_TO_OFFSET_DISPLAY = 2;

	public static ArrayList<String> get1WeekDatesIsoFormat(String dateSelectedIsoFormat) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormatUtils.ISO_DATE_FORMAT.getPattern());
		Date date = sdf.parse(dateSelectedIsoFormat);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(GregorianCalendar.DAY_OF_YEAR, -DAYS_TO_OFFSET_DISPLAY);

		ArrayList<String> results = new ArrayList<String>();
		for (int i = 0; i < 7; i++){
			cal.getTime();
			results.add(DateFormatUtils.ISO_DATE_FORMAT.format(cal));
			cal.add(GregorianCalendar.DAY_OF_YEAR, 1);
		}

		return(results);
	}

	public static String getProviderFullNameHtmlEscaped(String providerId) throws NotAuthorisedException_Exception{
		ProviderTransfer tempResult = OscarProviderManager.getProvider(providerId);
		StringBuilder sb = new StringBuilder();

		if (tempResult.getFirstName() != null) sb.append(tempResult.getFirstName());

		if (tempResult.getLastName() != null){
			if (sb.length() > 0) sb.append(' ');

			sb.append(tempResult.getLastName());
		}

		return(sb.toString());
	}
}
