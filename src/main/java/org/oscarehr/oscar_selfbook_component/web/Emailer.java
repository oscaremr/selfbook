/**
* Copyright (c) 2016 - selfbook.ca 
*
* This software is published under the GPL GNU General Public License.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version. 
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*
* This software was written by Tan Pham for
* selfbook.ca, Saint-Laurent, Quebec, Canada to be provided
* as part of the OSCAR McMaster EMR System
**/

package org.oscarehr.oscar_selfbook_component.web;

/**
 *
 * @author tan
 */
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.mail.EmailException;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.oscarehr.util.VelocityUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.Provider;
import org.oscarehr.oscar_selfbook_component.manager.OscarProviderManager;
import org.oscarehr.oscar_selfbook_component.manager.OscarScheduleManager;
import org.oscarehr.oscar_selfbook_component.utils.EmailUtils;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.util.LocaleUtils;
import org.oscarehr.ws.AppointmentTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;
import org.oscarehr.oscar_selfbook_component.utils.EncryptionUtils;
 /**
 *
 * @author tan
 */
public final class Emailer {

    
    public static String clinicFolder = ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "clinic_folder");
    public static String senderName = ConfigXmlUtils.getPropertyString("sendmail", "senderName");
    public static String senderEmail = ConfigXmlUtils.getPropertyString("sendmail", "senderEmail");
    public static String bccEmail = ConfigXmlUtils.getPropertyString("sendmail", "bccEmail");
    public static String receptionEmail = ConfigXmlUtils.getPropertyString("sendmail", "receptionEmail");
    public static String selfBookEndPointExternalURLPrefix = ConfigXmlUtils.getPropertyString("oscar_selfbook_portal", "selfBookEndPointExternalURLPrefix");

    public void sendEmailToConfirmAppt(int demoId, String firstName, String lastName, int appointmentId, String numberOfPerson, Calendar startTime, String patientemail, String providerName, String providerLine, String messageln1, String messageln2, Locale locale) throws EmailException {

        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.Log4JLogChute");
        ve.setProperty("runtime.log.logsystem.log4j.logger", "myvelocity.log");
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        Template t = ve.getTemplate(clinicFolder + "/toconfirmappt_" + locale.getLanguage() + ".html", "UTF-8");
        Template tplaintxtMsg = ve.getTemplate(clinicFolder + "/toconfirmappt_" + locale.getLanguage() + ".txt", "UTF-8");
        VelocityContext velocityContext = VelocityUtils.createVelocityContextWithTools();
            
        String link = String.valueOf(appointmentId);
        String confirmationlink = "";
        String cancellationlink = "";
        String demoIdLink = "";
        Clinic clinic=null;
        try {
            clinic = Clinic.getClinic();
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }
        String apptDateTime = dateToString(startTime, clinic.getTimeZone(),locale);
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        try {
            confirmationlink = myEncryptionUtils.encrypt("con" + locale.getLanguage() + link);
            cancellationlink = myEncryptionUtils.encrypt("can" + locale.getLanguage() + link);
            demoIdLink = myEncryptionUtils.encrypt("did" + locale.getLanguage() + String.valueOf(demoId));
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }

        velocityContext.put("firstName", firstName);
        velocityContext.put("lastName", lastName);
        velocityContext.put("appointmentId", appointmentId);
        velocityContext.put("patientemail", patientemail);
        velocityContext.put("apptDateTime", apptDateTime);
        velocityContext.put("numberOfPerson", numberOfPerson);
        velocityContext.put("confirmationlink", confirmationlink);
        velocityContext.put("cancellationlink", cancellationlink);
        velocityContext.put("demoIdLink", demoIdLink);
        velocityContext.put("providerName", providerName);
        velocityContext.put("providerLine", providerLine);
        velocityContext.put("messageln1", messageln1);
        velocityContext.put("messageln2", messageln2);
        velocityContext.put("selfBookEndPointExternalURLPrefix", selfBookEndPointExternalURLPrefix);
        velocityContext.put("clinicMainWebLink", clinic!=null?clinic.getMainWebLink():"");
        velocityContext.put("clinicTitle", clinic!=null?LocaleUtils.getMessage(locale,clinic.getTitle()):"");
        velocityContext.put("clinicAddress", clinic!=null?clinic.getAddress():"");
        velocityContext.put("clinicPhone", clinic!=null?clinic.getPhone():"");

        StringWriter writer = new StringWriter();
        StringWriter writer1 = new StringWriter();
        t.merge(velocityContext, writer);

        String htmlMessageTemplate = writer.toString();
        tplaintxtMsg.merge(velocityContext, writer1);

        String txtMessageTemplate = writer1.toString();
        String receipientEmailAddress = patientemail;
        String receipientName = firstName + " " + lastName;
        String fromEmailAddress = senderEmail;
        String fromName = senderName;
        String subject = LocaleUtils.getMessage(locale, "MedApptFor") + " " + receipientName;
        String toBccEmailAddress = bccEmail;
        EmailUtils.sendEmail(receipientEmailAddress, receipientName, fromEmailAddress, fromName, toBccEmailAddress, subject, txtMessageTemplate, htmlMessageTemplate);

    }

    public void sendEmailToReception(int demoId, String firstName, String lastName, int appointmentId, String numberOfPerson, Calendar startTime, String patientemail, String providerName, String providerLine, String messageln1, String messageln2, String receptionMessage, Locale locale) throws EmailException {

        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.Log4JLogChute");
        ve.setProperty("runtime.log.logsystem.log4j.logger", "myvelocity.log");
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        Template t = ve.getTemplate(clinicFolder + "/toconfirmappt_" + locale.getLanguage() + ".html", "UTF-8");
        Template tplaintxtMsg = ve.getTemplate(clinicFolder + "/toconfirmappt_" + locale.getLanguage() + ".txt", "UTF-8");
        VelocityContext velocityContext = VelocityUtils.createVelocityContextWithTools();

    
        String link = String.valueOf(appointmentId);
        String confirmationlink = "";
        String cancellationlink = "";
        String demoIdLink = "";
        Clinic clinic=null;
        try {
            clinic = Clinic.getClinic();
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }
        String apptDateTime = dateToString(startTime, clinic.getTimeZone(),locale);
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        try {
            confirmationlink = myEncryptionUtils.encrypt("con" + locale.getLanguage() + link);
            cancellationlink = myEncryptionUtils.encrypt("can" + locale.getLanguage() + link);
            demoIdLink = myEncryptionUtils.encrypt("did" + locale.getLanguage() + String.valueOf(demoId));
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }

        velocityContext.put("firstName", firstName);
        velocityContext.put("lastName", lastName);
        velocityContext.put("appointmentId", appointmentId);
        velocityContext.put("patientemail", patientemail);
        velocityContext.put("apptDateTime", apptDateTime);
        velocityContext.put("numberOfPerson", numberOfPerson);
        velocityContext.put("confirmationlink", confirmationlink);
        velocityContext.put("cancellationlink", cancellationlink);
        velocityContext.put("demoIdLink", demoIdLink);
        velocityContext.put("providerName", providerName);
        velocityContext.put("providerLine", providerLine);
        velocityContext.put("messageln1", messageln1);
        velocityContext.put("messageln2", messageln2);
        velocityContext.put("selfBookEndPointExternalURLPrefix", selfBookEndPointExternalURLPrefix);
        velocityContext.put("clinicMainWebLink", clinic!=null?clinic.getMainWebLink():"");
        velocityContext.put("clinicTitle", clinic!=null?LocaleUtils.getMessage(locale,clinic.getTitle()):"");
        velocityContext.put("clinicAddress", clinic!=null?clinic.getAddress():"");
        velocityContext.put("clinicPhone", clinic!=null?clinic.getPhone():"");
        
        StringWriter writer = new StringWriter();
        StringWriter writer1 = new StringWriter();
        t.merge(velocityContext, writer);

        String htmlMessageTemplate = writer.toString();
        tplaintxtMsg.merge(velocityContext, writer1);

        String txtMessageTemplate = writer1.toString();
        String receipientEmailAddress = receptionEmail;
        String receipientName = "";
        String fromEmailAddress = senderEmail;
        String fromName = senderName;
        String subject = receptionMessage + " " + firstName + " " + lastName;
        String toBccEmailAddress = null;
        EmailUtils.sendEmail(receipientEmailAddress, receipientName, fromEmailAddress, fromName, toBccEmailAddress, subject, txtMessageTemplate, htmlMessageTemplate);

    }

    public void sendConfirmedApptEmail(Locale locale, int apptId) throws EmailException {

        String messageln1 = "";
        String messageln2 = "";
        String apptDateTime = "", ptName = "", ptemail = "";
        int demographicNo = 0;
        String providerName = "";
        String providerLine = "";
        String reason;
        Clinic clinic = null;
        OscarScheduleManager myOscarScheduleManager = new OscarScheduleManager();
        try {
            clinic = Clinic.getClinic();
            AppointmentTransfer theAppointment = myOscarScheduleManager.getAppointment(apptId);
            if (theAppointment != null) {
                Calendar startTime = theAppointment.getAppointmentStartDateTime();
                apptDateTime = dateToString(startTime, clinic.getTimeZone(),locale);
                ptName = theAppointment.getName();
                if (ptName.contains(",")) {
                    String[] parts = ptName.split(",");
                    if (parts != null && parts[0] != null && parts[1] != null) {
                        ptName = parts[1] + " " + parts[0];
                    }
                }

                ptName = ptName.replace(",", " ");
                ptemail = theAppointment.getResources();
                demographicNo = theAppointment.getDemographicNo();
                reason = theAppointment.getReason();
                ProviderTransfer provider = OscarProviderManager.getProvider(theAppointment.getProviderNo());
                Map<String, Provider> providers = clinic.getAllProviders();
                providerName=providers.get(theAppointment.getProviderNo()).getProviderName();
                if (provider.getProviderNo().equals(clinic.getWalkInDoctor())) {
                    providerLine = LocaleUtils.getMessage(locale, "WithWalkinDoctor");
                } else {
                    providerLine = LocaleUtils.getMessage(locale, "WithDoctor") + " " + providerName;
                }

                if (reason != null && reason.contains("FUAD")) { // attending physician
                    messageln1 = LocaleUtils.getMessage(locale, "ConfirmedAttendingDoctor");
                    messageln2 = LocaleUtils.getMessage(locale, "ApptCanBeRefused");
                } else if (reason != null && reason.contains("FUFD")) { //follow up family doctor
                    messageln1 = LocaleUtils.getMessage(locale, "ConfirmedFamDoctor");
                    messageln2 = LocaleUtils.getMessage(locale, "ApptCanBeRefused");
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);

        }

        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.Log4JLogChute");
        ve.setProperty("runtime.log.logsystem.log4j.logger", "myvelocity.log");
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        Template t = ve.getTemplate(clinicFolder + "/confirmedappt_" + locale.getLanguage() + ".html", "UTF-8");
        Template tplaintxtMsg = ve.getTemplate(clinicFolder + "/confirmedappt_" + locale.getLanguage() + ".txt", "UTF-8");
        VelocityContext velocityContext = VelocityUtils.createVelocityContextWithTools();

        String link = String.valueOf(apptId);
        String confirmationlink = "";
        String cancellationlink = "";
        String demoIdLink = "";
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        try {
            confirmationlink = myEncryptionUtils.encrypt("con" + locale.getLanguage() + link);
            cancellationlink = myEncryptionUtils.encrypt("can" + locale.getLanguage() + link);
            demoIdLink = myEncryptionUtils.encrypt("did" + locale.getLanguage() + String.valueOf(demographicNo));
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }

        velocityContext.put("ptName", ptName);
        velocityContext.put("appointmentId", apptId);
        velocityContext.put("patientemail", ptemail);
        velocityContext.put("apptDateTime", apptDateTime);
        velocityContext.put("confirmationlink", confirmationlink);
        velocityContext.put("cancellationlink", cancellationlink);
        velocityContext.put("demoIdLink", demoIdLink);
        velocityContext.put("providerName", providerName);
        velocityContext.put("providerLine", providerLine);
        velocityContext.put("messageln1", messageln1);
        velocityContext.put("messageln2", messageln2);
        velocityContext.put("selfBookEndPointExternalURLPrefix", selfBookEndPointExternalURLPrefix);        
        velocityContext.put("clinicMainWebLink", clinic!=null?clinic.getMainWebLink():"");
        velocityContext.put("clinicTitle", clinic!=null?LocaleUtils.getMessage(locale,clinic.getTitle()):"");
        velocityContext.put("clinicAddress", clinic!=null?clinic.getAddress():"");
        velocityContext.put("clinicPhone", clinic!=null?clinic.getPhone():"");

        StringWriter writer = new StringWriter();
        StringWriter writer1 = new StringWriter();
        t.merge(velocityContext, writer);

        String htmlMessageTemplate = writer.toString();
        tplaintxtMsg.merge(velocityContext, writer1);

        String txtMessageTemplate = writer1.toString();
        String receipientEmailAddress = ptemail;
        String receipientName = ptName;
        String fromEmailAddress = senderEmail;
        String fromName = senderName;
        String subject = LocaleUtils.getMessage(locale, "ConfirmedApptFor") + " " + ptName;
        String toBccEmailAddress = bccEmail;
        EmailUtils.sendEmail(receipientEmailAddress, receipientName, fromEmailAddress, fromName, toBccEmailAddress, subject, txtMessageTemplate, htmlMessageTemplate);

    }

    public void sendCanceledEmail(Locale locale, int apptId) throws EmailException {

        String apptDateTime = "", ptName = "", ptemail = "";
        int demographicNo = 0;
        String providerName = "";
        String providerLine = "";
        OscarScheduleManager myOscarScheduleManager = new OscarScheduleManager();
        Clinic clinic=null;
        try {
            clinic = Clinic.getClinic();
            AppointmentTransfer theAppointment = myOscarScheduleManager.getAppointment(apptId);
            if (theAppointment != null) {
                Calendar startTime = theAppointment.getAppointmentStartDateTime();
                apptDateTime = dateToString(startTime, clinic.getTimeZone(),locale);
                ptName = theAppointment.getName();
                if (ptName.contains(",")) {
                    String[] parts = ptName.split(",");
                    if (parts != null && parts[0] != null && parts[1] != null) {
                        ptName = parts[1] + " " + parts[0];
                    }
                }

                ptName = ptName.replace(",", " ");
                ptemail = theAppointment.getResources();
                demographicNo = theAppointment.getDemographicNo();
                ProviderTransfer provider = OscarProviderManager.getProvider(theAppointment.getProviderNo());
                Map<String, Provider> providers = clinic.getAllProviders();
                providerName=providers.get(theAppointment.getProviderNo()).getProviderName();
                if (provider.getProviderNo().equals(clinic.getWalkInDoctor())) {
                    providerLine = LocaleUtils.getMessage(locale, "WithWalkinDoctor");
                } else {
                    providerLine = LocaleUtils.getMessage(locale, "WithDoctor") + " " + providerName;
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);

        }

        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.Log4JLogChute");
        ve.setProperty("runtime.log.logsystem.log4j.logger", "myvelocity.log");
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        Template t = ve.getTemplate(clinicFolder + "/canceledappt_" + locale.getLanguage() + ".html", "UTF-8");
        Template tplaintxtMsg = ve.getTemplate(clinicFolder + "/canceledappt_" + locale.getLanguage() + ".txt", "UTF-8");
        VelocityContext velocityContext = VelocityUtils.createVelocityContextWithTools();

        String link = String.valueOf(apptId);
        String confirmationlink = "";
        String cancellationlink = "";
        String demoIdLink = "";
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        try {
            confirmationlink = myEncryptionUtils.encrypt("con" + locale.getLanguage() + link);
            cancellationlink = myEncryptionUtils.encrypt("can" + locale.getLanguage() + link);
            demoIdLink = myEncryptionUtils.encrypt("did" + locale.getLanguage() + String.valueOf(demographicNo));
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }

        velocityContext.put("ptName", ptName);
        velocityContext.put("appointmentId", apptId);
        velocityContext.put("patientemail", ptemail);
        velocityContext.put("apptDateTime", apptDateTime);
        velocityContext.put("confirmationlink", confirmationlink);
        velocityContext.put("cancellationlink", cancellationlink);
        velocityContext.put("demoIdLink", demoIdLink);
        velocityContext.put("providerName", providerName);
        velocityContext.put("providerLine", providerLine);
        velocityContext.put("selfBookEndPointExternalURLPrefix", selfBookEndPointExternalURLPrefix);
        velocityContext.put("clinicMainWebLink", clinic!=null?clinic.getMainWebLink():"");
        velocityContext.put("clinicTitle", clinic!=null?LocaleUtils.getMessage(locale,clinic.getTitle()):"");
        velocityContext.put("clinicAddress", clinic!=null?clinic.getAddress():"");
        velocityContext.put("clinicPhone", clinic!=null?clinic.getPhone():"");
        StringWriter writer = new StringWriter();
        StringWriter writer1 = new StringWriter();
        t.merge(velocityContext, writer);

        String htmlMessageTemplate = writer.toString();
        tplaintxtMsg.merge(velocityContext, writer1);

        String txtMessageTemplate = writer1.toString();
        String receipientEmailAddress = ptemail;
        String receipientName = ptName;
        String fromEmailAddress = senderEmail;
        String fromName = senderName;
        String subject = LocaleUtils.getMessage(locale, "ApptCanceledFor") + " " + ptName;
        String toBccEmailAddress = bccEmail;
        EmailUtils.sendEmail(receipientEmailAddress, receipientName, fromEmailAddress, fromName, toBccEmailAddress, subject, txtMessageTemplate, htmlMessageTemplate);

    }

    public String confirmAppt(String conf) throws EmailException {
        Integer apptId;
        String message="";
        String apptIdStr = "";
        String decryptedText;

        Locale locale = Locale.ENGLISH;
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        Clinic clinic=null;
        try {
            clinic = Clinic.getClinic();
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            decryptedText = myEncryptionUtils.decrypt(conf);
            if (decryptedText.length() > 5 && decryptedText.substring(0, 3).equals("con")) {
                apptIdStr = decryptedText.substring(5);
            }
            if (decryptedText.length() > 5 && decryptedText.substring(3, 5).equals("fr")) {
                locale = Locale.FRENCH;
            }

            apptId = Integer.parseInt(apptIdStr);

            OscarScheduleManager myOscarScheduleManager = new OscarScheduleManager();
            AppointmentTransfer theAppointment = myOscarScheduleManager.getAppointment(apptId);
            if (theAppointment == null) {
                message = LocaleUtils.getMessage(locale, "ApptNotFound");
            } else {
                Calendar startTime = theAppointment.getAppointmentStartDateTime();
                Calendar currentTime = Calendar.getInstance();
                String apptDateTime = dateToString(startTime, clinic.getTimeZone(),locale);
                String status = theAppointment.getStatus();

                if (currentTime.after(startTime)) {
                    message = LocaleUtils.getMessage(locale, "NoNeedToConfirmAlreadyPassed");
                } else if (theAppointment.getStatus().equals("t")) {
                    OscarScheduleManager.updateAppointmentStatus(apptId, "T");
                    message = LocaleUtils.getMessage(locale, "YourApptOn") + " " + apptDateTime + " " + LocaleUtils.getMessage(locale, "IsNowConfirmed");
                    sendConfirmedApptEmail(locale, apptId);
                } else if (theAppointment.getStatus().equals("C")) {
                    message = LocaleUtils.getMessage(locale, "CannotConfirmAlreadyCanceled");
                } else if (theAppointment.getStatus().equals("T")) {
                    message = LocaleUtils.getMessage(locale, "YourApptOn") + " " + apptDateTime + " " + LocaleUtils.getMessage(locale, "IsAlreadyConfirmed");
                } else if (status.equals("H") || status.equals("P") || status.equals("E")) {
                    message = LocaleUtils.getMessage(locale, "NoNeedToConfirmWasAtClinic");
                } else if (status.equals("N")) {
                    OscarScheduleManager.updateAppointmentStatus(apptId, "T");
                    message = LocaleUtils.getMessage(locale, "YourApptOn") + " " + apptDateTime + " " + LocaleUtils.getMessage(locale, "IsNowConfirmed");
                    sendConfirmedApptEmail(locale, apptId);
                }
            }
        } catch (NotAuthorisedException_Exception ex) {
            message = LocaleUtils.getMessage(locale, "ErrorTryLater");
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            message = LocaleUtils.getMessage(locale, "ValidationError");
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }

    public String cancelAppt(String conf) throws EmailException {
        Integer apptId;
        String message = "";
        String apptIdStr = "";
        String decryptedText;
        Locale locale = Locale.ENGLISH;
        EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
        Clinic clinic=null;
        try {
            clinic = Clinic.getClinic();
        } catch (Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            decryptedText = myEncryptionUtils.decrypt(conf);
            if (decryptedText.length() > 5 && decryptedText.substring(0, 3).equals("can")) {
                apptIdStr = decryptedText.substring(5);
            }

            if (decryptedText.length() > 5 && decryptedText.substring(3, 5).equals("fr")) {
                locale = Locale.FRENCH;
            }

            apptId = Integer.parseInt(apptIdStr);

            OscarScheduleManager myOscarScheduleManager = new OscarScheduleManager();
            AppointmentTransfer theAppointment = myOscarScheduleManager.getAppointment(apptId);
            if (theAppointment != null) {
                Calendar startTime = theAppointment.getAppointmentStartDateTime();
                Calendar currentTime = Calendar.getInstance();
                String apptDateTime = dateToString(startTime, clinic.getTimeZone(),locale);                
                String status = theAppointment.getStatus();

                if (currentTime.after(startTime)) {
                    message = LocaleUtils.getMessage(locale, "NoNeedToCancelAlreadyPassed");
                } else if (theAppointment.getStatus().equals("t")) {
                    OscarScheduleManager.updateAppointmentStatus(apptId, "C");
                    message = LocaleUtils.getMessage(locale, "YourApptOn") + " " + apptDateTime + " " + LocaleUtils.getMessage(locale, "IsNowCanceled");
                    sendCanceledEmail(locale, apptId);
                } else if (theAppointment.getStatus().equals("T")) {
                    OscarScheduleManager.updateAppointmentStatus(apptId, "C");
                    message = LocaleUtils.getMessage(locale, "YourApptOn") + " " + apptDateTime + " " + LocaleUtils.getMessage(locale, "IsNowCanceled");

                    sendCanceledEmail(locale, apptId);
                } else if (theAppointment.getStatus().equals("C")) {
                    message = LocaleUtils.getMessage(locale, "YourApptOn") + " " + apptDateTime + " " + LocaleUtils.getMessage(locale, "IsAlreadyCanceled");
                } else if (status.equals("H") || status.equals("P") || status.equals("E")) {
                    message = LocaleUtils.getMessage(locale, "NoNeedToCancelWasAtClinic");
                } else if (status.equals("N")) {
                    OscarScheduleManager.updateAppointmentStatus(apptId, "C");
                    message = LocaleUtils.getMessage(locale, "YourApptOn") + " " + apptDateTime + " " + LocaleUtils.getMessage(locale, "IsNowCanceled");
                    sendCanceledEmail(locale, apptId);
                }
            }
        } catch (NotAuthorisedException_Exception ex) {
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            message = LocaleUtils.getMessage(locale, "ValidationError");
            Logger.getLogger(Emailer.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }
    
       public String dateToString(Calendar myCalendar,String timeZone, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, YYYY-MM-dd, hh:mm a",locale);
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        return sdf.format(myCalendar.getTime());
       }
}
