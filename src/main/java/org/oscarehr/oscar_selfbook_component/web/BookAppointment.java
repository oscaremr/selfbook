/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar_selfbook_component.web;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import org.apache.commons.mail.EmailException;

import org.apache.log4j.Logger;
import org.oscarehr.util.LocaleUtils;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.oscar_selfbook_component.booking.Clinic;
import org.oscarehr.oscar_selfbook_component.booking.Provider;
import org.oscarehr.oscar_selfbook_component.manager.OscarDemographicManager;
import org.oscarehr.oscar_selfbook_component.manager.OscarProviderManager;
import org.oscarehr.oscar_selfbook_component.manager.OscarScheduleManager;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProviderTransfer;

public final class BookAppointment{
	public static final String MYOSCAR_APPOINTMENT_MEDICAL_DATA_TYPE = "OSCAR.appointment";
	private static final Logger logger = MiscUtils.getLogger();

        public static String removeAccents(String text) {
            return text==null ?null: Normalizer.normalize(text,Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        }
        
	public static Integer bookAppointment(int demoId,String firstName, String lastName, String phone, String email, String ip, Calendar startTime, String providerId, Long appointmentTypeId, Character code, String notes,String reason, String nam,Locale locale) throws Exception{
      
        DemographicTransfer oscarDemographic = null;
        String messageln1 = "";
        String messageln2 = "";
        String famDocNo = "";
        String providerLine = "";
        logger.debug("provider :" + providerId + " appointmentId :" + appointmentTypeId + " code " + code + " notes " + notes);

        Calendar endTime;
        endTime = (Calendar) startTime.clone();
        String orgfirstName = firstName;
        String orglastName = lastName;
        firstName = removeAccents(firstName);
        firstName = firstName.trim();
        lastName = removeAccents(lastName);
        lastName = lastName.trim();
        String messageErreur = "";
        Emailer myEmailer = new Emailer();
        String providerName="";
        String location="";
         ProviderTransfer provider=null;
        try {
            Clinic clinic = Clinic.getClinic();
            Map<String, Provider> providers = clinic.getAllProviders();
            provider = OscarProviderManager.getProvider(providers.get(providerId).getProviderNo());
            location=clinic.getLocation();
            providerName=providers.get(providerId).getProviderName();
            int durationMinutes = clinic.getAppointmentDuration(providerId, appointmentTypeId, code);

            endTime.add(Calendar.MINUTE, durationMinutes);
            endTime.getTimeInMillis();

            if (provider.getProviderNo().equals(clinic.getWalkInDoctor())) {
                providerLine = LocaleUtils.getMessage(locale, "WithWalkinDoctor");
            } else {
                providerLine = LocaleUtils.getMessage(locale, "WithDoctor") + " " + providerName;
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(BookAppointment.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (demoId <= 0) {
            notes += " " + nam;
            messageErreur += LocaleUtils.getMessage(locale, "HIN") + ":" + nam;
        } else {
            oscarDemographic = OscarDemographicManager.getDemographicByDemographicNo(demoId);

            if (oscarDemographic != null) {
                famDocNo = oscarDemographic.getProviderNo();

                if (!oscarDemographic.getFirstName().equals(firstName)) {
                    messageErreur += LocaleUtils.getMessage(locale, "FirstName") + ": " + firstName;
                }
                if (!oscarDemographic.getLastName().equals(lastName)) {
                    messageErreur += LocaleUtils.getMessage(locale, "Name") + ": " + lastName;
                }
                if (!oscarDemographic.getEmail().equals(email)) {
                    messageErreur += LocaleUtils.getMessage(locale, "Email") + " ";
                }
                if (!(oscarDemographic.getPhone().equals(phone) || oscarDemographic.getPhone2().equals(phone))) {
                    messageErreur += LocaleUtils.getMessage(locale, "Phone") + " ";
                }
            }
        }
        if (messageErreur.length() > 0) {
            messageErreur = "Check" + messageErreur;
        }
        Integer resultId = bookAppointmentInOscar(demoId, orgfirstName, orglastName, phone, email, ip, startTime, provider.getProviderNo(), messageErreur, endTime, reason,location);
        if (reason != null && reason.contains("FUAD")) {//follow up attending doctor
            messageln1 = LocaleUtils.getMessage(locale, "ConfirmedAttendingDoctor");
            messageln2 = LocaleUtils.getMessage(locale, "ApptCanBeRefused");
        } else if (reason != null && reason.contains("FUFD")) { //follow up family doctor
            messageln1 = LocaleUtils.getMessage(locale, "ConfirmedFamDoctor");
            messageln2 = LocaleUtils.getMessage(locale, "ApptCanBeRefused");
            if (oscarDemographic == null) {

                String txtMessage = LocaleUtils.getMessage(locale, "HINNotInSystem");
                try {
                    myEmailer.sendEmailToReception(demoId, firstName, lastName, resultId, reason, startTime, email, providerName, providerLine, messageln1, messageln2, txtMessage, locale);
                } catch (EmailException ex) {
                    logger.error("error sending mail",ex);
                }
            } else if (!famDocNo.equals(providerId)) {

                String txtMessage = LocaleUtils.getMessage(locale, "NotFamDoctor");
                try {
                    myEmailer.sendEmailToReception(demoId, firstName, lastName, resultId, reason, startTime, email, providerName, providerLine, messageln1, messageln2, txtMessage, locale);
                } catch (EmailException ex) {
                    //java.util.logging.Logger.getLogger(BookAppointment.class.getName()).log(Level.SEVERE, null, ex);
                     logger.error("error sending mail",ex);
                }
            }
        }

        try {
            myEmailer.sendEmailToConfirmAppt(demoId, firstName, lastName, resultId, reason, startTime, email, providerName, providerLine, messageln1, messageln2, locale);
        } catch (EmailException ex) {
            java.util.logging.Logger.getLogger(BookAppointment.class.getName()).log(Level.SEVERE, null, ex);
        }

        return (resultId);
    }

 
        private static Integer bookAppointmentInOscar(int demoId,String firstName, String lastName, String phone, String email, String ip,  Calendar startTime, String providerId, String notes, Calendar endTime,String reason,String location) throws NotAuthorisedException_Exception, org.oscarehr.ws.NotAuthorisedException_Exception{
                notes+=" "+ip;
                
		Integer resultId = OscarScheduleManager.addAppointment(startTime, endTime, demoId, lastName+","+firstName, notes, providerId,reason,phone,location,email);
		return resultId;
	}

}