<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.OscarServerWebServicesManager"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.oscarehr.oscar_selfbook_component.manager.OscarProviderManager"%>
<%@page import="java.util.Map"%>
<%@page import="org.oscarehr.oscar_selfbook_component.web.Emailer"%>
<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List"%>
<%@page import="org.oscarehr.oscar_selfbook_component.manager.AppointmentType"%>
<%@page	import="org.oscarehr.oscar_selfbook_component.manager.OscarDemographicManager"%>
<%@page import="org.oscarehr.ws.DemographicTransfer"%>
<%@page import="org.oscarehr.ws.AppointmentTransfer"%>
<%@page import="org.oscarehr.util.WebUtils"%>
<%@page import="org.oscarehr.ws.ProviderTransfer"%>
<%@page	import="org.oscarehr.oscar_selfbook_component.booking.*"%>
<%
    Locale locale = Locale.ENGLISH;

    if ("fr".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "fr");
    } else if ("en".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "en");
    }
    if (session.getAttribute("lang") == null) {
        session.setAttribute("lang", "en");
    }
    if (session.getAttribute("lang").equals("fr")) {
        locale = Locale.FRENCH;
    }

    try {
            OscarProviderManager.getProviders(true);
        } catch (Exception e) {
            //force relogin  for expired security token
            OscarServerWebServicesManager.initLoginResultTransfer();
        }
    
    Clinic clinic = Clinic.getClinic();

    clinic.setAppointmentBookingOnline(true);

    if (!clinic.isAppointmentBookingOnline()) {
        session.setAttribute("error", LocaleUtils.getMessage(locale, "Appointment_Service_Offline"));
        response.sendRedirect("../message.jsp");
        return;
    }

    String demo_idencrypted = "";
    if (request.getParameter("demo_id") != null) {
        demo_idencrypted = request.getParameter("demo_id");
    }
%>

<!DOCTYPE html>
<html>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <head>
        <title><%=LocaleUtils.getMessage(locale, clinic.getTitle())%> - <%=LocaleUtils.getMessage(locale, "OnlineBooking")%></title>
        <!-- Bootstrap -->
        <script src="<%=request.getContextPath()%>/js/jquery.js"></script>
        <script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
        <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <style type="text/css">	
            .loading{
                display:block;
            }		
        </style>

        <script type="text/javascript">
            $(document).ajaxStart(function () {
                $("#availAppts").hide();
                $("#loadingAvailAppts").addClass("loading");
                $("#loadingAvailAppts").show();
            });

            $(document).ajaxStop(function () {
                $("#loadingAvailAppts").removeClass("loading");
                $("#loadingAvailAppts").hide();
                $("#availAppts").show();
            });

            function getAppts2(appointmentTypeId, demoid) {
                $('#availAppts').load('find_appointment.jsp?appointmentTypeId=' + appointmentTypeId + '&demoid=' + demoid);
            }

            function myTrim(x) {
                return x.replace(/^\s+|\s+$/gm, '');
            }
            
            function checkhinqc(currentQuestion) {
                var nam = $("#a" + currentQuestion.toString()).val();
                nam = myTrim(nam);
                nam = nam.replace(/ /g, '');
                nam = nam.toUpperCase();
                $("#idnam").val(nam);
                $("#a" + currentQuestion.toString()).val(nam);
                if (nam === null || nam === '') {
                    alert("<%=LocaleUtils.getMessage(locale, "HINRequired")%>");
                    return(false);
                }
                var patt = new RegExp("[A-Z]{4}[0-9]{8}");
                if (nam.length != 12 || !patt.test(nam)) {
                    alert("<%=LocaleUtils.getMessage(locale, "HINInvalid")%>");
                    return(false);
                }
                return true;
            }
            
            function checkhinbc(currentQuestion) {
                var nam = $("#a" + currentQuestion.toString()).val();
                nam = myTrim(nam);
                nam = nam.replace(/ /g, '');
                nam = nam.toUpperCase();
                $("#idnam").val(nam);
                $("#a" + currentQuestion.toString()).val(nam);
                if (nam === null || nam === '') {
                    alert("<%=LocaleUtils.getMessage(locale, "HINRequired")%>");
                    return(false);
                }
                var patt = new RegExp("[0-9]{10}");
                if (nam.length != 10 || !patt.test(nam)) {
                    alert("<%=LocaleUtils.getMessage(locale, "HINInvalid")%>");
                    return(false);
                }
                return true;

            }
            function checkhin(currentQuestion) {
                var nam = $("#a" + currentQuestion.toString()).val();
                nam = myTrim(nam);
                nam = nam.replace(/ /g, '');
                nam = nam.toUpperCase();
                $("#idnam").val(nam);
                $("#a" + currentQuestion.toString()).val(nam);
                if (nam == null || nam == '') {
                    alert("<%=LocaleUtils.getMessage(locale, "HINRequired")%>");
                    return(false);
                }
			// BC and Ontario
				var patt = new RegExp("[0-9]{10}");
			// Quebec
                var qc = new RegExp("[A-Z]{4}[0-9]{8}");
			// Alberta, Manitoba, New Brunswick, PEI, Saskatchewan, Nunavut
				var ot = new RegExp("[0-9]{9}");
			// NWT
				var nwt = new RegExp("[NDMT]{1}[0-9]{7}");
                if ((nam.length != 10 || !patt.test(nam)) 
					&& (nam.length != 12 || !qc.test(nam)) 
					&& (nam.length != 9 || !ot.test(nam))
					&& (nam.length != 8 || !nwt.test(nam))) {
                    	alert("<%=LocaleUtils.getMessage(locale, "HINInvalid")%>");
                    		return(false);
                }
                return true;

            }
            
            function beforedays(currentQuestion, days) {
                var mydate = $("#a" + currentQuestion.toString()).val();
                if (mydate != null && mydate.length > 0) {
                    var d = moment(mydate, "YYYY-MM-DD");
                    var t = moment();
                    d = d.add(days, "d");
                    return d.isAfter(t);
                }
                return false;
            }

            function validatedate(currentQuestion) {
                var mydate = $("#a" + currentQuestion.toString()).val();
                if (mydate != null && mydate.length > 0) {
                    var d = moment(mydate, "YYYY-MM-DD");
                    var t = moment();
                    if (!d.isValid())
                    {
                        alert("<%=LocaleUtils.getMessage(locale, "EnterDate")%>");
                        return false;
                    }
                    else if (!t.isAfter(d)) {
                        alert("<%=LocaleUtils.getMessage(locale, "FuturistDate")%>");
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                return false;


            }


            var currentQuestion = 1;

            function gotonextquestion(nextQuestion)
            {
                $("#q" + currentQuestion.toString()).hide();
                currentQuestion = nextQuestion;
                $("#q" + nextQuestion.toString()).show();
                if ($("#at" + currentQuestion.toString()).val() == "end")
                {
                    $("#nextbt").hide();
                }
                if ($("#sq" + currentQuestion.toString()).val() != "")
                {
                    $("#q" + $("#sq" + currentQuestion.toString()).val()).show();
                }

                var at = $("#at" + currentQuestion.toString()).val();

                if (at == "listprov" && $("#listprov" + currentQuestion.toString() + " option").length == "2")
                {
                    $("#listprov" + currentQuestion.toString())[0].selectedIndex = 1;
                    if ($("#sq" + currentQuestion.toString()).val() == "")
                    {
                        validatedata();
                    }
                }
            }

            function validatedata()
            {
                var at = $("#at" + currentQuestion.toString()).val();
                var vt = $("#vt" + currentQuestion.toString()).val();
                if (at == "text")
                {
                    if (vt == "hinqc" && (checkhinqc(currentQuestion) == false)) {
                        return false;
                    }
                    else if (vt == "hinbc" && (checkhinbc(currentQuestion) == false)) {
                        return false;
                    }
                    else if (vt == "hin" && (checkhin(currentQuestion) == false)) {
                        return false;
                    }
                    else {
                        appType += currentQuestion.toString() + ";" + $("#a" + currentQuestion.toString()).val() + ";";
                        var nq = $("#nq" + currentQuestion.toString()).val();
                        gotonextquestion(parseInt(nq));
                    }
                }
                if (at == "radio")
                {
                    var selectedVal = "";
                    var selected = $("#q" + currentQuestion.toString() + " input[type='radio']:checked");
                    if (selected.length > 0) {
                        selectedVal = selected.val();
                        appType += currentQuestion.toString() + ";" + selectedVal + ";";
                        gotonextquestion(parseInt(selectedVal));
                    }
                }
                if (at == "date")
                {
                    var selectedVal = "";
                    var selected = $("#q" + currentQuestion.toString() + " input[type='hidden']:checked");
                    if (validatedate(currentQuestion) == false) {
                        return false;
                    }
                    if (beforedays(currentQuestion, $("#bd" + currentQuestion.toString()).val()) == true) {
                        appType += currentQuestion.toString() + ";" + $("#a" + currentQuestion.toString()).val() + ";";

                        var nq = $("#vdnq" + currentQuestion.toString()).val();
                        gotonextquestion(parseInt(nq));
                    } else {
                        var nq = $("#nq" + currentQuestion.toString()).val();
                        gotonextquestion(parseInt(nq));
                    }
                }
                if (at == "listprov")
                {
                    var checked = true;
                    if ($("#sq" + currentQuestion.toString()).val() != "")

                    {
                        checked = $("#chxbx" + $("#sq" + currentQuestion.toString()).val()).is(':checked');
                    }
                    if (checked) {
                        $("#nextbt").hide();
                        $("#chxbx" + $("#sq" + currentQuestion.toString()).val()).prop("disabled", true);
                        $("#repQuest").val(currentQuestion + "-" + appType);

                        var sel = $("#listprov" + currentQuestion.toString()).val();
                        $("#idMdFam").val(sel);
                        $("#listprov" + currentQuestion.toString()).prop("disabled", true);
                        getAppts('', '<%=demo_idencrypted%>');
                        $("#afficherrv").show();
                        $("#rvdisp").show();
                        $("#availAppts").show();
                    }
                    else {
                        alert("<%=LocaleUtils.getMessage(locale, "NotTraitant")%>");
                    }
                }
            }

            var appType = "";


            function getAppts(srtd, demoid) {
                document.getElementById("availAppts").innerHTML = "";
                $('#availAppts').scrollTop(0);
                $('#availAppts').load('find_appointment.jsp?srtD=' + srtd + '&demo_id=' + demoid,
                        $('#apptForm').serialize(), function () {
                    $('#availAppts').scrollTop(0);
                });
            }
            
            function confirmAppts(urlid, demoid) {
                $("#nextbt").hide();
                $('#rvdisp').hide();
                $('#rvdispbs').hide();
                $('#rvchoisi').show();
                $('#availAppts').load('confirm_booking2.jsp?id=' + urlid + '&demo_id=' + demoid);
                return(false);
            }

            function bookAppts() {
                $('#availAppts').load('confirm_booking_action.jsp', $('confirmApptForm').serialize());
            }

            function checkbookinguserinfo()
            {
                $("#firstName").val($.trim($("#firstName").val()));
                $("#lastName").val($.trim($("#lastName").val()));
                $("#phone").val($.trim($("#phone").val()));
                $("#email").val($.trim($("#email").val()));
                if ($("#lastName").val().length == 0) {
                    alert("<%=LocaleUtils.getMessage(locale, "FillLastName")%>");
                } else if ($("#firstName").val().length == 0) {
                    alert("<%=LocaleUtils.getMessage(locale, "FillFirstName")%>");

                } else if ($("#phone").val().length == 0) {
                    alert("<%=LocaleUtils.getMessage(locale, "FillPhone")%>");

                } else if ($("#email").val().length == 0) {
                    alert("<%=LocaleUtils.getMessage(locale, "FillEmail")%>");
                } else if (!validateEmail($("#email").val())) {
                    alert("<%=LocaleUtils.getMessage(locale, "FillValidEmail")%>");
                }
                else {
                    $("#bookapptbtn").prop("disabled", true);
                    $("form#confirmApptForm").submit();
                }
            }
            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        </script>    
    </head>
    <body>
        <div class="container-fluid">
            <div class="row-fluid">  
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                    <a href="<%=clinic.getMainWebLink()%>"><img src="../img/<%=clinic.getLogoFileName()%>" alt="Logo" class="img-responsive" ><b><%=LocaleUtils.getMessage(locale, clinic.getTitle())%></b>, <%=clinic.getAddress()%>,  Tel: <%=clinic.getPhone()%></a><p>
                </div>
                <div class="col-xs-10 col-sm-12 col-md-4 col-md-offset-3">

                    <h2><%=LocaleUtils.getMessage(locale, "OnlineBooking")%></h2>
                    <div class="btn-group">
                        <button type="button" class="btn btn-large" onclick="document.location.href = '<%=request.getContextPath()%>/index.jsp'"  ><%=LocaleUtils.getMessage(locale, "Restart")%></button>
                    </div>

                    <form  onsubmit="return(false)" id="apptForm">
                        <br>
                        <%
                            Map<String, Algorithm> algorithms = clinic.getAllAlgorithms();
                            Algorithm algorithm = null;
                            int algSize = algorithms.size();
                            for (int i = 1; i <= algSize; i++) {
                                algorithm = algorithms.get(String.valueOf(i));
                        %>
                        <div id="q<%=i%>" <%=i > 1 ? "style='display: none'" : ""%>>
                            <label>
                                <% if ("checkbox".equals(algorithm.getAnswerType())) {%>    
                                <input type="checkbox" name="chkbx<%=i%>" id="chxbx<%=i%>" value="" ><%}%>
                                <%=LocaleUtils.getMessage(locale, algorithm.getDisplayText())%>
                            </label>
                            <% if ("text".equals(algorithm.getAnswerType())) {%>
                            <input id="a<%=i%>" class="form-control"> <%}%>
                            <% if ("date".equals(algorithm.getAnswerType())) {
                                    Map<String, Integer> answerDateBeforeDays = algorithm.getAnswerDateBeforeDays();
                                    for (String answerDateBeforeDay : answerDateBeforeDays.keySet()) {
                            %>
                            <input  type="hidden" id="vdnq<%=i%>" value="<%=answerDateBeforeDays.get(answerDateBeforeDay)%>">
                            <input  type="hidden" id="bd<%=i%>" value="<%=answerDateBeforeDay%>">
                            <%}%>    
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker<%=i%>'>
                                    <input type='text' id="a<%=i%>" class="form-control" size="10"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker<%=i%>').datetimepicker({locale: "<%=session.getAttribute("lang")%>", format: "YYYY-MM-DD"});
                                });
                            </script>
                            <%}%>
                            <% if ("radio".equals(algorithm.getAnswerType())) {
                                    Map<String, Integer> answerRadioDisplayTexts = algorithm.getAnswerRadioDisplayTexts();
                                    for (String answerRadioDisplayText : answerRadioDisplayTexts.keySet()) {

                            %>
                            <div class="radio">
                                <label><input type="radio" name="ar<%=i%>" value="<%=answerRadioDisplayTexts.get(answerRadioDisplayText)%>" ><%=LocaleUtils.getMessage(locale, answerRadioDisplayText)%></label>
                            </div>
                            <%}
                                }%>
                            <% if ("listprov".equals(algorithm.getAnswerType())) {%>        
                            <select class="form-control" id="listprov<%=i%>" >
                                <option value=""></option>
                                <%
                                    Map<String, Provider> providers = clinic.getAllProviders();
                                %>
                                <%
                                    List<String> listProvName = new ArrayList<String>();
                                    for (String providerAlias : providers.keySet()) {
                                        ProviderTransfer provider = OscarProviderManager.getProvider(providers.get(providerAlias).getProviderNo());
                                        if (algorithm.getValidation().contains(" " + providerAlias + " ")) {
                                            if (provider.getProviderNo().equals(clinic.getWalkInDoctor())) {
                                                listProvName.add(LocaleUtils.getMessage(locale, "WalkIn") + ":" + providerAlias);
                                            } else {
                                                listProvName.add(providers.get(providerAlias).getProviderName()+ ":" + providerAlias);
                                                
                                            }
                                        }
                                    }
                                    java.util.Collections.sort(listProvName);

                                    for (String temp : listProvName) {
                                %>

                                <option value="<%=temp.substring(temp.indexOf(":") + 1, temp.length())%>"><%=temp.substring(0, temp.indexOf(":"))%></option>
                                <%}%>
                            </select>
                            <%}%>

                        </div>
                        <input type="hidden" id="at<%=i%>" value="<%=algorithm.getAnswerType()%>">
                        <input type="hidden" id="vt<%=i%>" value="<%=algorithm.getValidation()%>">
                        <input type="hidden" id="nq<%=i%>" value="<%=algorithm.getNextQuestionNo()%>">
                        <input type="hidden" id="sq<%=i%>" value="<%=algorithm.getSubQuestionNo()%>">
                        <%}%>
                        <input type=hidden name="nam" id="idnam" >    
                        <input type=hidden name="idMdFam" id="idMdFam" >    
                        <div id="ntrt" >                
                            <label><br>
                                <input type="button" name="next" id="nextbt" value="<%=LocaleUtils.getMessage(locale, "Next")%>" onclick="validatedata();">
                            </label>
                        </div>        
                        <input type="hidden" name="repQuest" id="repQuest" value="">
                        <input type="hidden" name="appointmentTypeId" value="1">
                </div>    
            </div>
            <div id="afficherrv" style="display: none">               
                <div class="row-fluid">
                    <div class="col-xs-10 col-sm-12 col-md-4 col-md-offset-3">
                        <div id="rvdisp" style="display: none" >
                            <br>
                            <%="true".equals(clinic.getDisplayMessageCanAlsoCallToBookAppt())?LocaleUtils.getMessage(locale, "CanCallToMakeAppt"):""%>
                            <h3><%=LocaleUtils.getMessage(locale, "AvailableAppointments")%></h3>
                        </div>
                        <div id="rvdispbs" style="display: none" >
                            <br>
                            <%="true".equals(clinic.getDisplayMessageCanAlsoCallToBookAppt())?LocaleUtils.getMessage(locale, "CanCallBS"):""%>
                            <h3><%=LocaleUtils.getMessage(locale, "AvailableAppointment")%></h3>
                        </div>
                        <div id="rvchoisi" style="display: none">
                            <h3><%=LocaleUtils.getMessage(locale, "ChosenAppt")%></h3>
                        </div>
                        <div id="loadingAvailAppts" style="margin: 0px; padding: 0px; height: 300px; overflow: auto; display:none;">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    <span class="sr-only"><%=LocaleUtils.getMessage(locale, "SearchingAppt")%></span>
                                </div>
                            </div>
                        </div>
                        <div id="availAppts" style="margin: 0px; padding: 0px; width: 100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


