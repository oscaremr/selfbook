<%--

Copyright (c) 2016 - selfbook.ca 

This software is published under the GPL GNU General Public License.
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

This software was written by Tan Pham for
selfbook.ca, Saint-Laurent, Quebec, Canada to be provided
as part of the OSCAR McMaster EMR System

--%>

<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="org.oscarehr.oscar_selfbook_component.web.Emailer"%>
<%
    String conf = null;
    if (request.getParameter("conf") != null) {
        conf = request.getParameter("conf");
    }
    try {
        Emailer myEmailer = new Emailer();
        String message = myEmailer.cancelAppt(conf);
        session.setAttribute("message", message);
        response.sendRedirect("../message.jsp");
        return;
    } catch (Exception e) {
        MiscUtils.getLogger().error("Unexpected error", e);
    }
    session.setAttribute("error", LocaleUtils.getMessage(request, "AppointmentBookingFailure"));
    response.sendRedirect("../message.jsp");
    return;
%>