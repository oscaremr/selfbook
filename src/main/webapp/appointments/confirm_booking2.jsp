<%--

Copyright (c) 2016 - selfbook.ca 

This software is published under the GPL GNU General Public License.
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

This software was written by Tan Pham for
selfbook.ca, Saint-Laurent, Quebec, Canada to be provided
as part of the OSCAR McMaster EMR System

--%>


<%@page import="org.oscarehr.oscar_selfbook_component.booking.Provider"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.TimeFormats"%>
<%@page import="org.oscarehr.oscar_selfbook_component.web.ViewWeekAppointmentSlots"%>
<%@page import="org.oscarehr.oscar_selfbook_component.manager.OscarDemographicManager"%>
<%@page import="org.oscarehr.ws.DemographicTransfer"%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.EncryptionUtils"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.Algorithm"%>
<%@page import="java.util.Map"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.TimeSlot"%>
<%@page import="java.util.Locale"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.Clinic"%>
<%

    Clinic clinic = Clinic.getClinic();
    Locale locale = Locale.ENGLISH;

    if ("fr".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "fr");
    } else if ("en".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "en");
    }
    if (session.getAttribute("lang") == null) {
        session.setAttribute("lang", "en");
    }
    if (session.getAttribute("lang").equals("fr")) {
        locale = Locale.FRENCH;
    }
    String combinedStrEnc = request.getParameter("id");
    TimeSlot timeslot = clinic.decryptTimeSlot(combinedStrEnc);
    String providerId = timeslot.getProviderNo();
    String nam = timeslot.getNam();
    String repQuest = timeslot.getRepQuest();
    String repQuestLong = "";
    Map<String, Algorithm> algorithms = clinic.getAllAlgorithms();
    Algorithm algorithm = null;
    int algSize = algorithms.size();
    String qNo = repQuest.substring(0, repQuest.indexOf('-'));
    int questionNo = Integer.parseInt(qNo);
    if (questionNo <= algSize) {
        algorithm = algorithms.get(String.valueOf(questionNo));
        repQuestLong = LocaleUtils.getMessage(locale, algorithm.getApptDesc());
        Pattern p = Pattern.compile("(\\d{4}-\\d{2}-\\d{2})");
        Matcher m = p.matcher(repQuest);
        if (m.find() == true) {
            String datestr = repQuest.substring(m.start(), m.end());
            repQuestLong += " " + datestr;
        }
    } else {
        //error
        String message = LocaleUtils.getMessage(locale, "UnexpectedError");
        session.setAttribute("message", message);
        response.sendRedirect("../message.jsp");
        return;
    }

    String demoidStrEnc = "";
    String demoidStr = "";
    int demoid = 0;
    if (request.getParameter("demo_id") != null) {
        demoidStrEnc = request.getParameter("demo_id");
    }
    EncryptionUtils myEncryptionUtils = new EncryptionUtils();
    if (demoidStrEnc.length() > 0) {
        String decryptedText = myEncryptionUtils.decrypt(demoidStrEnc);
        if (decryptedText.length() > 3 && decryptedText.substring(0, 3).equals("did")) {
            demoidStr = decryptedText.substring(3);
        }

        try {
            demoid = Integer.parseInt(demoidStr);
        } catch (Exception e) {
            MiscUtils.getLogger().error("Unexpected error", e);
            demoid = 0;
        }
    }

    DemographicTransfer oscarDemographic = null;
    if (demoid != 0) {
        oscarDemographic = OscarDemographicManager.getDemographicByDemographicNo(demoid);
    }
    
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, YYYY-MM-dd, hh:mm a",locale);
        sdf.setTimeZone(TimeZone.getTimeZone(clinic.getTimeZone()));
        String datetimeDisplay = sdf.format(timeslot.getAvailableApptTime().getTime());
        
        Map<String, Provider> providers = clinic.getAllProviders();
        String providerName=providers.get(providerId).getProviderName();
        
%>
<form id="confirmApptForm" class="form-horizontal" role="form" action="confirm_booking_action.jsp" method="post">

    <div class="row">
        <div class="col-xs-4"><%=LocaleUtils.getMessage(locale, "Provider")%></div>
        <div class="col-xs-8"><strong><%=providerId.equals(clinic.getWalkInDoctor()) ? LocaleUtils.getMessage(locale, "WalkIn") : providerName%></strong></div>
    </div>

    <div class="row">
        <div class="col-xs-4"><%=LocaleUtils.getMessage(locale, "Date")%></div>
        <div class="col-xs-8"><strong><%=datetimeDisplay%></strong></div>
    </div>
    <div class="row">
        <div class="col-xs-4"><%=LocaleUtils.getMessage(locale, "AppointmentType")%>:</div>
        <div class="col-xs-8"><strong><%=repQuestLong%></strong></div>
    </div>  <br>
    <div class="form-group">
        <label for="Nam" class="col-sm-2 control-label"><%=LocaleUtils.getMessage(locale, "HIN")%></label>
        <div class="col-sm-10">
            <%=nam%>
        </div>
    </div>
    <div class="form-group">
        <label for="Nom" class="col-sm-2 control-label"><%=LocaleUtils.getMessage(locale, "Name")%></label>
        <div class="col-sm-10">
            <% if (demoid == 0) {%><input name="lastName" class="form-control"  id="lastName"><% } else {%><input type="hidden" name="lastName" id="lastName" value="<%=oscarDemographic.getLastName()%>">
            <%=oscarDemographic.getLastName()%><%}%>
        </div>
    </div> 

    <div class="form-group">
        <label for="Prenom" class="col-sm-2 control-label"><%=LocaleUtils.getMessage(locale, "FirstName")%></label>
        <div class="col-sm-10">
            <% if (demoid == 0) {%><input name="firstName" class="form-control"   id="firstName"><% } else {%><input type="hidden" name="firstName" id="firstName" value="<%=oscarDemographic.getFirstName()%>">
            <%=oscarDemographic.getFirstName()%> <%}%>
        </div>
    </div>

    <div class="form-group">
        <label for="Tel" class="col-sm-2 control-label"><%=LocaleUtils.getMessage(locale, "Phone")%></label>
        <div class="col-sm-10">
            <input name="phone" id="phone" class="form-control"  value="<%=demoid == 0 ? "" : oscarDemographic.getPhone()%>" >
        </div>
    </div>

    <div class="form-group">
        <label for="Courriel" class="col-sm-2 control-label"><%=LocaleUtils.getMessage(locale, "Email")%></label>
        <div class="col-sm-10">
            <input name="email" type="email" id="email"   class="form-control" value="<%=demoid == 0 ? "" : oscarDemographic.getEmail()%>" >
        </div>
    </div>   
    <div class="form-group">
        <label for="Note" class="col-sm-2 control-label"><%=LocaleUtils.getMessage(locale, "Reason")%></label>
        <div class="col-sm-10">
            <textarea name="appointmentNotes" class=" form-control"  rows="2"  maxlength="100" style="resize: none;"  ></textarea>
        </div>
    </div> 

    <div class="form-group">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-10">
            <button type="button" class="btn btn-primary" id="bookapptbtn" onclick="checkbookinguserinfo();"><%=LocaleUtils.getMessage(request, "BookThisAppointment")%></button>
        </div>
    </div> 
    <input type="hidden" name="id" value="<%=combinedStrEnc%>" />
    <input type="hidden" name="demo_id" value="<%=request.getParameter("demo_id")%>" >
</form> 
