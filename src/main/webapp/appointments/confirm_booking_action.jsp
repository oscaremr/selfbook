<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.jsoup.safety.Whitelist"%>
<%@page import="org.jsoup.Jsoup"%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.TimeFormats"%>
<%@page import="org.oscarehr.oscar_selfbook_component.manager.OscarDemographicManager"%>
<%@page import="org.oscarehr.oscar_selfbook_component.web.BookAppointment"%>
<%@page import="org.oscarehr.ws.DemographicTransfer"%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.EncryptionUtils"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.Algorithm"%>
<%@page import="java.util.Map"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.TimeSlot"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.Clinic"%>
<%@page import="java.util.Locale"%>
<%
        Locale locale = Locale.ENGLISH;

            if ("fr".equals(request.getParameter("lang"))) {
                session.setAttribute("lang", "fr");
            } else if ("en".equals(request.getParameter("lang"))) {
                session.setAttribute("lang", "en");
            }
            if (session.getAttribute("lang") == null) {
                session.setAttribute("lang", "en");
            }
            if (session.getAttribute("lang").equals("fr")) {
                locale = Locale.FRENCH;
            }

            String referer = request.getHeader("referer");
            String xforwardedfor = request.getHeader("x-forwarded-for");

            String notesComments = null;
            String firstName = null;
            String lastName = null;
            String phone = null;
            String email = null;
            String ipAddress = null;
            ipAddress = request.getRemoteAddr();
            if (xforwardedfor != null) {
                ipAddress = xforwardedfor;
            }
            int demo_id = 0;
            String combinedStrEnc = request.getParameter("id");
            String demoidStrEnc = "";
            if (request.getParameter("demo_id") != null) {
                demoidStrEnc = request.getParameter("demo_id");
            }

            if (request.getParameter("appointmentNotes") != null) {
                notesComments = request.getParameter("appointmentNotes");
            }
            if (request.getParameter("firstName") != null) {
                firstName = request.getParameter("firstName");
            }
            if (request.getParameter("lastName") != null) {
                lastName = request.getParameter("lastName");
            }
            if (request.getParameter("phone") != null) {
                phone = request.getParameter("phone");
            }
            if (request.getParameter("email") != null) {
                email = request.getParameter("email");
            }
            if (notesComments != null) {
                notesComments = Jsoup.clean(notesComments, Whitelist.none());
            }
            try {
                Clinic clinic = Clinic.getClinic();

                TimeSlot timeslot = clinic.decryptTimeSlot(combinedStrEnc);
                String reason = clinic.getAppointmentType(timeslot.getAppointmentType()).getName();
                String demoidStr = "";
                String nam = timeslot.getNam();
                String repQuest = timeslot.getRepQuest();
                String apptType = "";
                Map<String, Algorithm> algorithms = clinic.getAllAlgorithms();
                Algorithm algorithm = null;
                int algSize = algorithms.size();
                String qNo = repQuest.substring(0, repQuest.indexOf('-'));
                int questionNo = Integer.parseInt(qNo);
                if (questionNo <= algSize) {
                    algorithm = algorithms.get(String.valueOf(questionNo));
                    apptType = algorithm.getApptType();
                }
                Pattern p = Pattern.compile("(\\d{4}-\\d{2}-\\d{2})");
                Matcher m = p.matcher(repQuest);
                if (m.find() == true) {
                    String datestr = repQuest.substring(m.start(), m.end());
                    apptType += datestr;
                }

                EncryptionUtils myEncryptionUtils = new EncryptionUtils();
                if (demoidStrEnc.length() > 0) {
                    String decryptedText = myEncryptionUtils.decrypt(demoidStrEnc);
                    if (decryptedText.length() > 3 && decryptedText.substring(0, 3).equals("did")) {
                        demoidStr = decryptedText.substring(3);
                    }
                    if (demoidStr.length() > 0) {
                        demo_id = Integer.parseInt(demoidStr);
                    }
                }

                DemographicTransfer oscarDemographic = OscarDemographicManager.getDemographicByNam(nam);
                if (oscarDemographic != null) {
                    demo_id = oscarDemographic.getDemographicNo();
                }
                Integer appointmentId = BookAppointment.bookAppointment(demo_id, firstName, lastName, phone, email, ipAddress, timeslot.getAvailableApptTime(), timeslot.getProviderNo(), timeslot.getAppointmentType(), timeslot.getCode(), reason, apptType + " " + notesComments, timeslot.getNam(), locale);

                if (appointmentId != null) {

                    session.removeAttribute("appointmentTypeId");
                    session.removeAttribute("providerId");
                    session.removeAttribute("dateSelectedString");

                    SimpleDateFormat sdf = new SimpleDateFormat("EEEE, YYYY-MM-dd, hh:mm a",locale);
                    sdf.setTimeZone(TimeZone.getTimeZone(clinic.getTimeZone()));

                    String message = LocaleUtils.getMessage(locale, "AppointmentBookingSuccess") + " " + sdf.format(timeslot.getAvailableApptTime().getTime()) + "<br/>" + LocaleUtils.getMessage(locale, "ConfirmationEmailSentTo")
                            + " " + email
                            + LocaleUtils.getMessage(locale, "IfNoConfirmation");

                    session.setAttribute("message", message);
                    session.setAttribute("referer", referer);
                    response.sendRedirect("../message.jsp");
                    return;
                }
            } catch (Exception e) {
                MiscUtils.getLogger().error("Unexpected error", e);
            }
            session.setAttribute("error", LocaleUtils.getMessage(request, "AppointmentBookingFailure"));
            response.sendRedirect("../message.jsp");
            return;
%>