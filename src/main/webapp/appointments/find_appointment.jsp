<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.EncryptionUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="java.lang.reflect.Method"%>
<%@page import="org.oscarehr.ws.DayWorkScheduleTransfer"%>
<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.oscarehr.oscar_selfbook_component.web.ViewWeekAppointmentSlots"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date,java.net.URLEncoder"%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.TimeFormats,org.oscarehr.ws.ProviderTransfer"%>
<%@page import="org.oscarehr.util.WebUtils"%>
<%@page import="org.oscarehr.ws.ProviderTransfer"%>
<%@page import="org.oscarehr.ws.AppointmentTransfer" %>
<%@page import="org.oscarehr.ws.DemographicTransfer,org.oscarehr.ws.NotAuthorisedException_Exception" %>
<%@page import="org.oscarehr.oscar_selfbook_component.manager.AppointmentBookingManager" %>
<%@page import="org.oscarehr.oscar_selfbook_component.manager.OscarDemographicManager" %>
<%@page import="java.text.SimpleDateFormat,java.util.Collections,java.util.Comparator,org.oscarehr.oscar_selfbook_component.booking.*,org.oscarehr.oscar_selfbook_component.booking.filters.*" %>
<table width="100%;">
<%
    Locale locale=Locale.ENGLISH;
    
    if("fr".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "fr");
    } else if("en".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "en");
    } 
    if(session.getAttribute("lang")==null) {
        session.setAttribute("lang", "en");
    }
    if(session.getAttribute("lang").equals("fr")) {
        locale=Locale.FRENCH;
    } 
    

Long appointmentTypeId = Long.parseLong(request.getParameter("appointmentTypeId"));
session.setAttribute("appointmentTypeId",appointmentTypeId);
String providerAlias = request.getParameter("idMdFam");
String nam = request.getParameter("nam");
session.setAttribute("nam",nam);
String repQuest = request.getParameter("repQuest");
session.setAttribute("repQuest",repQuest);

session.setAttribute("providerAlias",providerAlias);

String  demo_idencrypted="",demo_id="";
        if(request.getParameter("demo_id")!=null) {
            demo_idencrypted=request.getParameter("demo_id");
        }

Clinic clinic = Clinic.getClinic();


if (  !clinic.isProviderAcceptingAppointments(providerAlias) ){	%>
<%=LocaleUtils.getMessage(request, "ProviderNotAcceptingOnlineBookings")%>
<%
return;
}

Calendar startDate = Calendar.getInstance();
startDate.setTimeZone(TimeZone.getTimeZone(clinic.getTimeZone()));
startDate.set(Calendar.HOUR_OF_DAY, 0);
startDate.set(Calendar.MINUTE, 0);
startDate.set(Calendar.SECOND, 0);
startDate.set(Calendar.MILLISECOND,0);
//Is this
EncryptionUtils myEncryptionUtils =  new EncryptionUtils();
if( request.getParameter("srtD") != null && !"undefined".equals(request.getParameter("srtD"))){
	try{
		String srtDenc = request.getParameter("srtD");
		String strD = myEncryptionUtils.decrypt(srtDenc);
		startDate = TimeFormats.parseJustDateFormat(strD);
	}catch(Exception e){
		
	}
}


List<TimeSlot> appointments=null;

DemographicTransfer oscarDemographic = OscarDemographicManager.getDemographicByNam(nam);
if (oscarDemographic != null) {
     if(!oscarDemographic.getChartNo().toUpperCase().endsWith(clinic.getNoOnlineBooking().toUpperCase()))
     {
         appointments= AppointmentBookingManager.findAppointments(clinic,providerAlias, appointmentTypeId, startDate);
     }
} else
{
    appointments= AppointmentBookingManager.findAppointments(clinic,providerAlias, appointmentTypeId, startDate);
}

int dayYear = -1;
Calendar lastDateSearched = null;
Calendar previousStartDate = null;
if(appointments!=null){
for(TimeSlot app:appointments){
    
	Calendar availTime = app.getAvailableApptTime();
	if(dayYear == -1 || dayYear != availTime.get(Calendar.DAY_OF_YEAR)){
		dayYear = availTime.get(Calendar.DAY_OF_YEAR);%>
		<tr>
			<th align="left"><%=TimeFormats.getWeekDay(locale, availTime) %><span style="float:right"><%=TimeFormats.getDayMonthYearExt(locale, availTime) %></th>
		</tr>
	<%}
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(clinic.getTimeZone()));
        String timeDisplay = sdf.format(availTime.getTime());
	app.setAppointmentType(appointmentTypeId);
        app.setNam(nam);
        app.setRepQuest(repQuest);
        app.setProviderNo(providerAlias);
	String encString = clinic.encrypt(app);
	lastDateSearched = app.getAvailableApptTime();
        if(previousStartDate==null) { previousStartDate= (Calendar) lastDateSearched.clone(); }
       
	%>
	<tr><td style="padding:0px;">
		<button class="btn btn-default btn-block " onclick="confirmAppts('<%=URLEncoder.encode(encString,"UTF-8")%>','<%=URLEncoder.encode(demo_idencrypted,"UTF-8")%>');">
		<%=timeDisplay %>
		</button>
	</td></tr>
	<%
}
}
/*
Next
Conditions to be handled.
if the number of appts > getNumberOfAppointmentOptionsToReturn use the next day after the last appt.
If the number of appts is less that the limit then we use startDate + daysToSearchAheadLimit
*/
Calendar nextStartDate = null;
if (lastDateSearched != null && appointments.size() >= clinic.getNumberOfAppointmentOptionsToReturn()){  
	nextStartDate = (Calendar) lastDateSearched.clone();
	nextStartDate.add(Calendar.DAY_OF_YEAR,1);
}else{
	nextStartDate = (Calendar) startDate.clone();
	nextStartDate.add(Calendar.DAY_OF_YEAR,clinic.getDaysToSearchAheadLimit());
}
%>

	<tr>
		<th>
                    <% if (appointments==null)
{%>
   <%=LocaleUtils.getMessage(locale, "CallClinicForAppts")%>
<%} else if(appointments.size()==0) 
{%>
   <%=LocaleUtils.getMessage(locale, "NoMoreAppts")%>
<%}%>                   
                    
        <p>&nbsp;<p>
                </th>
	</tr>
</table>