<%--

Copyright (c) 2016 - selfbook.ca 

This software is published under the GPL GNU General Public License.
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

This software was written by Tan Pham for
selfbook.ca, Saint-Laurent, Quebec, Canada to be provided
as part of the OSCAR McMaster EMR System

--%>
<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.Clinic"%>
<%@page import="java.util.Locale"%>
<%
    Locale locale = Locale.ENGLISH;
    if ("fr".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "fr");
    } else if ("en".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "en");
    }
    if (session.getAttribute("lang") == null) {
        session.setAttribute("lang", "en");
    }
    if (session.getAttribute("lang").equals("fr")) {
        locale = Locale.FRENCH;
    }
    Clinic clinic = Clinic.getClinic();
%>


<!DOCTYPE html>
<html>
    <meta name="viewport" content="width=device-width, initial-scale=1">     
    <head>
        <title><%=LocaleUtils.getMessage(locale, clinic.getTitle())%> - <%=LocaleUtils.getMessage(locale, "OnlineBooking")%></title>
        <!-- Bootstrap -->
        <script src="<%=request.getContextPath()%>/js/jquery.js"></script>
        <script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <style type="text/css">	
            .loading{
                display:block;
            }		
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row-fluid">  
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                    <a href="<%=clinic.getMainWebLink()%>"><img src="img/<%=clinic.getLogoFileName()%>" alt="Logo" class="img-responsive" ><b><%=LocaleUtils.getMessage(locale, clinic.getTitle())%></b>, <%=clinic.getAddress()%>,  Tel: <%=clinic.getPhone()%></a><p>
                </div>
                <div class="col-xs-10 col-sm-12 col-lg-12 col-md-6 col-md-offset-3">
                    <h2><%=LocaleUtils.getMessage(locale, "OnlineBooking")%></h2>
                    <p>
                    <div class="btn-group">
                        <button type="button" class="btn btn-large" onclick="document.location.href = '<%=request.getContextPath()%>/index.jsp'"  ><%=LocaleUtils.getMessage(locale, "Restart")%></button>
                    </div>
                </div>
                <div class="col-xs-10 col-sm-12 col-md-6 col-md-offset-3">
                    <div class="span7">
                        <div id="apptMessages"> 
                            <br>&nbsp;<br>
                            <%if (session.getAttribute("message") != null) {%>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <%=session.getAttribute("message")%>
                            </div>
                            <%
                                session.removeAttribute("message");
                            }%>

                            <%if (session.getAttribute("error") != null) {%>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <%=session.getAttribute("error")%>
                            </div>
                            <%
                                session.removeAttribute("error");
                            }%>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>