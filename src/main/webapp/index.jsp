<%--

Copyright (c) 2016 - selfbook.ca 

This software is published under the GPL GNU General Public License.
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

This software was written by Tan Pham for
selfbook.ca, Saint-Laurent, Quebec, Canada to be provided
as part of the OSCAR McMaster EMR System

--%>
<%@page import="java.util.Locale"%>
<%@page import="org.oscarehr.ws.AppointmentTransfer,java.text.SimpleDateFormat"%>
<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="java.util.Calendar,java.util.List"%>
<%@page import="org.oscarehr.oscar_selfbook_component.web.ViewWeekAppointmentSlots"%>
<%@page import="org.oscarehr.oscar_selfbook_component.utils.TimeFormats"%>
<%@page import="org.oscarehr.oscar_selfbook_component.booking.*"%>
<%@page import="org.jsoup.Jsoup" %>
<%@page import="org.jsoup.nodes.Document" %>
<%@page import="org.jsoup.safety.Whitelist" %>


<%
	Clinic clinic = Clinic.getClinic();
%>
<%

    Locale locale=Locale.ENGLISH;
    
    if("fr".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "fr");
    } else if("en".equals(request.getParameter("lang"))) {
        session.setAttribute("lang", "en");
    } 
    if(session.getAttribute("lang")==null) {
        session.setAttribute("lang", "en");
    }
    if(session.getAttribute("lang").equals("fr")) {
        locale=Locale.FRENCH;
    } 
%>


<!DOCTYPE html>

<html>
 <meta name="viewport" content="width=device-width, initial-scale=1">     
    <head>
	<title><%=LocaleUtils.getMessage(locale, clinic.getTitle())%> - <%=LocaleUtils.getMessage(locale, "OnlineBooking")%></title>
	<!-- Bootstrap -->
        	<script src="<%=request.getContextPath()%>/js/jquery.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
	<link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<style type="text/css">	
		.loading{
			display:block;
		}		
	</style>
</head>
<script type="text/javascript">
    function bookappt()
    {
       window.location.href = "appointments/bookAppointment.jsp"; 
    }
    </script>
    <body>
<div class="container-fluid">

					
      							
      							
<div class="row-fluid">  

    <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <a href="<%=clinic.getMainWebLink()%>"><img src="img/<%=clinic.getLogoFileName()%>" alt="Logo" class="img-responsive" ><b><%=LocaleUtils.getMessage(locale, clinic.getTitle())%></b>, <%=clinic.getAddress()%>,  Tel: <%=clinic.getPhone()%></a><p>
    </div>


   <div class="col-xs-10 col-sm-12  col-md-6 col-md-offset-3">
                
         <h2><%=LocaleUtils.getMessage(locale, "OnlineBooking")%></h2>
         <p>
              <div class="btn-group">
                  <%String sp = request.getServletPath(); %>
        <button type="button" class="btn btn-large" onclick="document.location.href='index.jsp?lang=<%=session.getAttribute("lang").equals("fr")?"en":"fr"%>'"  ><%=LocaleUtils.getMessage(locale, "InOtherLangage")%></button>

</div> 
        <p><br>
<%=LocaleUtils.getMessage(locale, "Introduction")%>.


    <p><p>

    <p><p><br><br>
        <%=LocaleUtils.getMessage(locale, "Conditions")%>
        <% if(LocaleUtils.getMessage(locale, "Condition1").isEmpty()==false) {%><span class="glyphicon glyphicon-ok"></span>&nbsp;<%=LocaleUtils.getMessage(locale, "Condition1")%><br><br><%}%>
        <% if(LocaleUtils.getMessage(locale, "Condition2").isEmpty()==false) {%><span class="glyphicon glyphicon-ok"></span>&nbsp;<%=LocaleUtils.getMessage(locale, "Condition2")%><br><br><%}%>
        <% if(LocaleUtils.getMessage(locale, "Condition3").isEmpty()==false) {%><span class="glyphicon glyphicon-ok"></span>&nbsp;<%=LocaleUtils.getMessage(locale, "Condition3")%><br><br><%}%>
        <% if(LocaleUtils.getMessage(locale, "Condition4").isEmpty()==false) {%><span class="glyphicon glyphicon-ok"></span>&nbsp;<%=LocaleUtils.getMessage(locale, "Condition4")%><br><br><%}%>
        <%=LocaleUtils.getMessage(locale, "Warning")%><br><br>
        
   <input type="button" name="next" id="next" value="<%=LocaleUtils.getMessage(locale, "MakeAppointment")%>" onclick="bookappt();">
   <p>&nbsp;<p>
     </div>
</div>
    </body>
</html>